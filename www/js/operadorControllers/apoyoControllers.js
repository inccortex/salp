angular.module('starter.apoyoController', [])

  .controller("nuevoApoyo_1Ctrl", function ($scope, $state, $stateParams, $ionicHistory, localBD, $ionicPopup, $ionicSlideBoxDelegate) {


    var loadData = function () {
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()

      $scope.ei = {tipo: $stateParams.eiTipo}

      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500,
      }

      $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
        // data.slider is the instance of Swiper
        $scope.slider = data.slider
      });

      $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
        //console.log('Slide change is beginning')
      });

      $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
        // note: the indexes are 0-based
        $scope.activeIndex = data.slider.activeIndex
        $scope.previousIndex = data.slider.previousIndex
      });


      $scope.pertenece = {
        cens: false,
        telecom: false,
        exclusivo: false,
        noaplica: false
      }

      $scope.apType = {
        concreto: false,
        metalico: false,
        fibra: false,
        riel: false,
        madera: false
      }

      $scope.chk = {
        a: '',
        b: '',
        c: '',
        d: '',
        e: '',
        f: ''
      }

      $scope.noAplica = function () {
        $scope.chk.a = false
        $scope.chk.b = false
        $scope.chk.c = false
        $scope.chk.d = false
        $scope.chk.e = false
        $scope.change()
      }

      $scope.apoyo = {
        pertenece: 0,
        v_pertenece: 0,
        tipo: 0,
        v_tipo: 0,
        longitud: '',
        v_longitud: 0,
        observacion: '',
        novedades: '',
        activo_id: $stateParams.idActivo,
        trafo_id: 0
      }

      $scope.change = function () {
        $scope.apoyo.novedades = ''
        if ($scope.chk.a) {
          $scope.apoyo.novedades += "Valla publicitaria,"
          $scope.chk.f = false
        }
        if ($scope.chk.b) {
          $scope.apoyo.novedades += "Poste desplomado,"
          $scope.chk.f = false
        }
        if ($scope.chk.c) {
          $scope.apoyo.novedades += "Poste quebrado,"
          $scope.chk.f = false
        }
        if ($scope.chk.d) {
          $scope.apoyo.novedades += "Poste caído,"
          $scope.chk.f = false
        }
        if ($scope.chk.e) {
          $scope.apoyo.novedades += "Poste corroído,"
          $scope.chk.f = false
        }
        if ($scope.chk.f) {
          $scope.apoyo.novedades = "No aplica"
        }
      }

    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    $scope.apPertenece = function () {
      $scope.apoyo.pertenece = 0
      $scope.apoyo.tipo = 0

      if ($scope.pertenece.cens) {
        $scope.pertenece.telecom = false
        $scope.pertenece.exclusivo = false
        $scope.apoyo.pertenece = 1
      } else if ($scope.pertenece.telecom) {
        $scope.pertenece.cens = false
        $scope.pertenece.exclusivo = false
        $scope.apoyo.pertenece = 2
        $scope.apoyo.tipo = 1
      } else if ($scope.pertenece.exclusivo) {
        $scope.pertenece.telecom = false
        $scope.pertenece.cens = false
        $scope.apoyo.pertenece = 3
      } else if ($scope.pertenece.noaplica) {
        $scope.pertenece.telecom = false
        $scope.pertenece.cens = false
        $scope.pertenece.exclusivo = false
        $scope.apoyo = {
          pertenece: 0,
          v_pertenece: 0,
          tipo: 0,
          v_tipo: 0,
          longitud: '',
          v_longitud: 0,
          observacion: 'NO APLICA - REFLECTOR',
          novedades: 'NO APLICA - REFLECTOR',
          activo_id: $stateParams.idActivo,
          trafo_id: 0
        }
        saveNoaplicaApp()
      }

    }

    $scope.apTipo = function () {
      $scope.apoyo.tipo = 0

      if ($scope.apType.concreto) {
        $scope.apType.metalico = false
        $scope.apType.fibra = false
        $scope.apType.riel = false
        $scope.apType.madera = false
        $scope.apoyo.tipo = 1
      } else if ($scope.apType.metalico) {
        $scope.apType.concreto = false
        $scope.apType.fibra = false
        $scope.apType.riel = false
        $scope.apType.madera = false
        $scope.apoyo.tipo = 2
      } else if ($scope.apType.fibra) {
        $scope.apType.concreto = false
        $scope.apType.metalico = false
        $scope.apType.riel = false
        $scope.apType.madera = false
        $scope.apoyo.tipo = 3
      } else if ($scope.apType.riel) {
        $scope.apType.concreto = false
        $scope.apType.metalico = false
        $scope.apType.fibra = false
        $scope.apType.madera = false
        $scope.apoyo.tipo = 4
      } else if ($scope.apType.madera) {
        $scope.apType.concreto = false
        $scope.apType.metalico = false
        $scope.apType.fibra = false
        $scope.apType.riel = false
        $scope.apoyo.tipo = 5
      }

    }


    $scope.finalizarApoyo = function () {

      if ($scope.apoyo.novedades !== '' && $scope.apoyo.tipo > 0 && $scope.apoyo.pertenece > 0 && $scope.apoyo.longitud !== '') {


        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea Finalizar el Apoyo?',
          template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para finalizar el registro del Apoyo y seguir con Elementos de Iluminación.'
        })
        confirmPopup.then(function (res) {
          if (res) {
            localBD.createApoyo($scope.apoyo).then(function (result) {

              if (result.insertId > 0) {

                //window.localStorage.setItem("apoyo", result.insertId)
                //console.log("eiTipo: " + $stateParams.eiTipo)
                //console.log("Vamo pa luminaria")
                $state.go('eiTipoEI', {
                  'idActivo': $stateParams.idActivo,
                  'idApoyo': result.insertId,
                  'eiTipo': $stateParams.eiTipo
                }, {reload: true})

              } else {
                $ionicPopup.alert({
                  title: '¡Ups hay un error!',
                  template: 'Hay un prblema interno con el dispositivo y los contraladores de almacenamiento, por favor reinicie la SALP e intentelo de nuevo.'
                })
                $ionicSlideBoxDelegate.slide(0, 500)
                $scope.reiniciar()
              }

            }, function (error) {
              console.error(error);
            })
          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe seleccionar los campos necesarios para finalizar y continuar'
        })
        $ionicSlideBoxDelegate.slide(0, 500)
        $scope.reiniciar()
      }

    }

    $scope.reiniciar = function () {
      $scope.apoyo.pertenece = 0
      $scope.apoyo.v_pertenece = 0
      $scope.apoyo.tipo = 0
      $scope.apoyo.v_tipo = false
      $scope.apoyo.longitud = ''
      $scope.apoyo.v_longitud = false
      $scope.apoyo.observacion = ''
      $scope.apoyo.novedades = ''
      $scope.apoyo.activo_id = $stateParams.idActivo
      $scope.apoyo.trafo_id = 0
      $scope.pertenece.cens = false
      $scope.pertenece.telecom = false
      $scope.pertenece.exclusivo = false
      $scope.apType.concreto = false
      $scope.apType.metalico = false
      $scope.apType.fibra = false
      $scope.apType.riel = false
      $scope.apType.madera = false
      $scope.chk.a = false
      $scope.chk.b = false
      $scope.chk.c = false
      $scope.chk.d = false
      $scope.chk.e = false
      $scope.chk.f = false
    }

    var saveNoaplicaApp = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: '¿Desea Finalizar el Apoyo como < NO APLICA >?',
        template: 'Por favor, verifique antes de realización esta acción, ya que la informaciónd el apoyo quedará como no aplica'
      })
      confirmPopup.then(function (res) {
        if (res) {
          localBD.createApoyo($scope.apoyo).then(function (result) {
            if (result.insertId > 0) {
              $state.go('eiTipoEI', {
                'idActivo': $stateParams.idActivo,
                'idApoyo': result.insertId,
                'eiTipo': $stateParams.eiTipo
              }, {reload: true})
            } else {
              $ionicPopup.alert({
                title: '¡Ups hay un error!',
                template: 'Hay un prblema interno con el dispositivo y los contraladores de almacenamiento, por favor reinicie la SALP e intentelo de nuevo.'
              })
              $ionicSlideBoxDelegate.slide(0, 500)
              $scope.reiniciar()
            }
          })
        } else {
          console.log('No desea continuar')
        }
      })
    }

  })
