angular.module('starter.eiFT', [])

  .controller("eiFTCtrl", function ($scope, $state, $stateParams, $ionicPopup, $cordovaCamera, $ionicHistory, localBD, $ionicSlideBoxDelegate) {

      var loadData = function () {
        $ionicHistory.clearCache()
        $ionicHistory.clearHistory()

        $scope.options = {
          loop: false,
          effect: 'fade',
          speed: 500,
        }

        $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
          // data.slider is the instance of Swiper
          $scope.slider = data.slider
        });

        $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
          //console.log('Slide change is beginning')
        });

        $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
          // note: the indexes are 0-based
          $scope.activeIndex = data.slider.activeIndex
          $scope.previousIndex = data.slider.previousIndex
        });

        console.log("DATA ei 2o4 " + $stateParams.eiTipo)
        $scope.ei = {
          tipo: $stateParams.eiTipo,
          tecnologia: '',
          v_tecnologia: false,
          nombre: '',
          potencia: '',
          v_potencia: false,
          foto: '',
          observacion: '',
          novedades: '',
          serial: '',
          apoyo_id: 0,
          activo_id: $stateParams.idActivo
        }

        $scope.pictureURl = ""


        $scope.tecnologia = {
          sodio: false,
          led: false,
          metal_halide: false,
          mercurio: false,
          luz_mixta: false
        }

        $scope.luminaria = {
          potenciaSodioA: false,
          potenciaSodioB: false,
          potenciaSodioC: false,
          potenciaSodioD: false,
          potenciaLedA: false,
          potenciaLedB: false,
          potenciaLedC: false,
          potenciaLedD: false,
          potenciaLedA20: false,
          potenciaLedA30: false,
          potenciaLedA32: false,
          potenciaLedA35: false,
          potenciaLedA36: false,
          potenciaLedA40: false,
          potenciaLedA42: false,
          potenciaLedB45: false,
          potenciaLedB46: false,
          potenciaLedB47: false,
          potenciaLedB50: false,
          potenciaLedB55: false,
          potenciaLedB60: false,
          potenciaLedB65: false,
          potenciaLedC70: false,
          potenciaLedC77: false,
          potenciaLedC90: false,
          potenciaLedD100: false,
          potenciaLedD120: false,
          potenciaMH70: false,
          potenciaMH120: false,
          potenciaMer125: false,
          potenciaMer250: false,
          potenciaMer400: false,
          potenciaLmx: false,
        }

        $scope.potenciaLed = ''

        $scope.farol = {
          potenciaSodio70: false,
          potenciaSodio150: false,
          potenciaSodio250: false,
          potenciaLed40: false,
          potenciaLed60: false,
        }

        $scope.reflector = {
          potenciaSodio: false
        }

        $scope.chk = {
          a: false,
          b: false,
          c: false,
          d: false,
          e: false,
          f: false,
          g: false
        }

        $scope.chkb = {
          a: false,
          b: false,
          c: false
        }

      }

      $scope.$on('$stateChangeSuccess', function (event, toState) {
        loadData();
      });


      $scope.eiTecnologia = function () {
        $scope.ei.tecnologia = ''

        if ($scope.tecnologia.sodio) {
          $scope.tecnologia.led = false
          /*$scope.tecnologia.metal_halide = false
          $scope.tecnologia.mercurio = false
          $scope.tecnologia.luz_mixta = false*/
          $scope.ei.tecnologia = "sodio"
        }
        if ($scope.tecnologia.led) {
          $scope.tecnologia.sodio = false
         /* $scope.tecnologia.metal_halide = false
          $scope.tecnologia.mercurio = false
          $scope.tecnologia.luz_mixta = false*/
          $scope.ei.tecnologia = "led"
        }
       /* if ($scope.tecnologia.metal_halide) {
          $scope.tecnologia.sodio = false
          $scope.tecnologia.led = false
          $scope.tecnologia.mercurio = false
          $scope.tecnologia.luz_mixta = false
          $scope.ei.tecnologia = "metal halide"
        }
        if ($scope.tecnologia.mercurio) {
          $scope.tecnologia.sodio = false
          $scope.tecnologia.led = false
          $scope.tecnologia.metal_halide = false
          $scope.tecnologia.luz_mixta = false
          $scope.ei.tecnologia = "mercurio"
        }
        if ($scope.tecnologia.luz_mixta) {
          $scope.tecnologia.sodio = false
          $scope.tecnologia.led = false
          $scope.tecnologia.metal_halide = false
          $scope.tecnologia.mercurio = false
          $scope.ei.tecnologia = "luz mixta"
        }*/

      }

      $scope.eiPostsodio = function () {

        $scope.ei.nombre = ''
        $scope.ei.potencia = ''
        $scope.potenciaLed = ''

        if ($scope.luminaria.potenciaSodioA) {
          $scope.luminaria.potenciaSodioB = false
          $scope.luminaria.potenciaSodioC = false
          $scope.luminaria.potenciaSodioD = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '70 W'
        }
        if ($scope.luminaria.potenciaSodioB) {
          $scope.luminaria.potenciaSodioA = false
          $scope.luminaria.potenciaSodioC = false
          $scope.luminaria.potenciaSodioD = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '150 W'
        }
        if ($scope.luminaria.potenciaSodioC) {
          $scope.luminaria.potenciaSodioA = false
          $scope.luminaria.potenciaSodioB = false
          $scope.luminaria.potenciaSodioD = false
          $scope.ei.nombre = 'C'
          $scope.ei.potencia = '250 W'
        }
        if ($scope.luminaria.potenciaSodioD) {
          $scope.luminaria.potenciaSodioA = false
          $scope.luminaria.potenciaSodioB = false
          $scope.luminaria.potenciaSodioC = false
          $scope.ei.nombre = 'D'
          $scope.ei.potencia = '250 W'
        }

      }

      $scope.eiPostLed = function () {
        $scope.potenciaLed = ''

        if ($scope.luminaria.potenciaLedA) {
          $scope.luminaria.potenciaLedB = false
          $scope.luminaria.potenciaLedC = false
          $scope.luminaria.potenciaLedD = false
          $scope.potenciaLed = 'A'
        }
        if ($scope.luminaria.potenciaLedB) {
          $scope.luminaria.potenciaLedA = false
          $scope.luminaria.potenciaLedC = false
          $scope.luminaria.potenciaLedD = false
          $scope.potenciaLed = 'B'
        }
        if ($scope.luminaria.potenciaLedC) {
          $scope.luminaria.potenciaLedA = false
          $scope.luminaria.potenciaLedB = false
          $scope.luminaria.potenciaLedD = false
          $scope.potenciaLed = 'C'
        }
        if ($scope.luminaria.potenciaLedD) {
          $scope.luminaria.potenciaLedA = false
          $scope.luminaria.potenciaLedB = false
          $scope.luminaria.potenciaLedC = false
          $scope.potenciaLed = 'D'
        }

      }

      $scope.eiPotenciaLed = function () {

        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.luminaria.potenciaLedA20) {
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '20 W'
        }
        if ($scope.luminaria.potenciaLedA30) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '30 W'
        }
        if ($scope.luminaria.potenciaLedA32) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '32 W'
        }
        if ($scope.luminaria.potenciaLedA35) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '35 W'
        }
        if ($scope.luminaria.potenciaLedA36) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '36 W'
        }
        if ($scope.luminaria.potenciaLedA40) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '40 W'
        }
        if ($scope.luminaria.potenciaLedA42) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'A'
          $scope.ei.potencia = '42 W'
        }
        if ($scope.luminaria.potenciaLedB45) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '45 W'
        }
        if ($scope.luminaria.potenciaLedB46) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '46 W'
        }
        if ($scope.luminaria.potenciaLedB47) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '47 W'
        }
        if ($scope.luminaria.potenciaLedB50) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '50 W'
        }
        if ($scope.luminaria.potenciaLedB55) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '55 W'
        }
        if ($scope.luminaria.potenciaLedB60) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '60 W'
        }
        if ($scope.luminaria.potenciaLedB65) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'B'
          $scope.ei.potencia = '65 W'
        }
        if ($scope.luminaria.potenciaLedC70) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'C'
          $scope.ei.potencia = '70 W'
        }
        if ($scope.luminaria.potenciaLedC77) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'C'
          $scope.ei.potencia = '77 W'
        }
        if ($scope.luminaria.potenciaLedC90) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'C'
          $scope.ei.potencia = '90 W'
        }
        if ($scope.luminaria.potenciaLedD100) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD120 = false
          $scope.ei.nombre = 'D'
          $scope.ei.potencia = '100 W'
        }
        if ($scope.luminaria.potenciaLedD120) {
          $scope.luminaria.potenciaLedA20 = false
          $scope.luminaria.potenciaLedA30 = false
          $scope.luminaria.potenciaLedA32 = false
          $scope.luminaria.potenciaLedA35 = false
          $scope.luminaria.potenciaLedA36 = false
          $scope.luminaria.potenciaLedA40 = false
          $scope.luminaria.potenciaLeda42 = false
          $scope.luminaria.potenciaLedB45 = false
          $scope.luminaria.potenciaLedB46 = false
          $scope.luminaria.potenciaLedB47 = false
          $scope.luminaria.potenciaLedB50 = false
          $scope.luminaria.potenciaLedB55 = false
          $scope.luminaria.potenciaLedB60 = false
          $scope.luminaria.potenciaLedB65 = false
          $scope.luminaria.potenciaLedC70 = false
          $scope.luminaria.potenciaLedC77 = false
          $scope.luminaria.potenciaLedC90 = false
          $scope.luminaria.potenciaLedD100 = false
          $scope.ei.nombre = 'D'
          $scope.ei.potencia = '120 W'
        }

      }

      $scope.eiPotenciaMH = function () {

        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.luminaria.potenciaMH120) {
          $scope.luminaria.potenciaMH70 = false
          $scope.ei.nombre = ''
          $scope.ei.potencia = '120 W'
        }
        if ($scope.luminaria.potenciaMH70) {
          $scope.luminaria.potenciaMH120 = false
          $scope.ei.nombre = ''
          $scope.ei.potencia = '70 W'
        }

      }

      $scope.eiPotenciaMer = function () {

        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.luminaria.potenciaMer400) {
          $scope.luminaria.potenciaMer125 = false
          $scope.luminaria.potenciaMer250 = false
          $scope.ei.nombre = ''
          $scope.ei.potencia = '400 W'
        }
        if ($scope.luminaria.potenciaMer250) {
          $scope.luminaria.potenciaMer125 = false
          $scope.luminaria.potenciaMer400 = false
          $scope.ei.nombre = ''
          $scope.ei.potencia = '250 W'
        }
        if ($scope.luminaria.potenciaMer125) {
          $scope.luminaria.potenciaMer250 = false
          $scope.luminaria.potenciaMer400 = false
          $scope.ei.nombre = ''
          $scope.ei.potencia = '125 W'
        }

      }

      $scope.eiPotenciaLMx = function () {
        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.luminaria.potenciaLmx) {
          $scope.ei.nombre = ''
          $scope.ei.potencia = '160 W'
        }
      }

      $scope.eiFarolPotSodio = function () {
        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.farol.potenciaSodio70) {
          $scope.farol.potenciaSodio150 = false
          $scope.farol.potenciaSodio250 = false
          $scope.ei.potencia = '70 W'
        }
        if ($scope.farol.potenciaSodio150) {
          $scope.farol.potenciaSodio70 = false
          $scope.farol.potenciaSodio250 = false
          $scope.ei.potencia = '150 W'
        }
        if ($scope.farol.potenciaSodio250) {
          $scope.farol.potenciaSodio70 = false
          $scope.farol.potenciaSodio150 = false
          $scope.ei.potencia = '250 W'
        }

      }

      $scope.eiFarolPotLed = function () {
        $scope.ei.nombre = ''
        $scope.ei.potencia = ''

        if ($scope.farol.potenciaLed40) {
          $scope.farol.potenciaLed60 = false
          $scope.ei.potencia = '40 W'
        }

        if ($scope.farol.potenciaLed60) {
          $scope.farol.potenciaLed40 = false
          $scope.ei.potencia = '60 W'
        }

      }

      $scope.tomarFoto = function () {
        var options = {
          destinationType: Camera.DestinationType.DATA_URL,
          encodingType: Camera.EncodingType.JPG,
          quality: 50,
          sourceType: 1
        }
        $cordovaCamera.getPicture(options)
          .then(function (data) {
            $scope.ei.foto = data
            $scope.pictureURl = 'data:image/jpg;base64,' + data
          }, function (error) {
            console.log('camera error:' + angular.toJson(error))
          })
      }


      $scope.noAplica = function () {
        $scope.chk.a = false
        $scope.chk.b = false
        $scope.chk.c = false
        $scope.chk.d = false
        $scope.chk.e = false
        $scope.chk.f = false
        $scope.chk.h = false
        $scope.chk.i = false
        $scope.change()
      }

      $scope.change = function () {
        $scope.ei.novedades = ''
        if ($scope.chk.a) {
          $scope.ei.novedades += "Iluminación caída,"
          $scope.chk.g = false
        }
        if ($scope.chk.b) {
          $scope.ei.novedades += "Sin bombillo,"
          $scope.chk.g = false
        }
        if ($scope.chk.c) {
          $scope.ei.novedades += "Directa,"
          $scope.chk.g = false
        }
        if ($scope.chk.d) {
          $scope.ei.novedades += "Elemento roto,"
          $scope.chk.g = false
        }
        if ($scope.chk.e) {
          $scope.ei.novedades += "Bombillo expuesto,"
          $scope.chk.g = false
        }
        if ($scope.chk.f) {
          $scope.ei.novedades += "Necesario Poda,"
          $scope.chk.g = false
        }
        if ($scope.chk.g) {
          $scope.ei.novedades = "No aplica,"
        }
        if ($scope.chk.h) {
          $scope.ei.novedades += "Elemento Privado,"
          $scope.chk.i = false
        }
        if ($scope.chk.i) {
          $scope.ei.novedades += "Elemento Público,"
          $scope.chk.h = false
        }
      }


      $scope.finalEi = function () {

        if ($scope.ei.tecnologia !== '' && $scope.ei.potencia !== '' && $scope.ei.foto !== '' && $scope.ei.novedades !== '' && ($scope.chk.i || $scope.chk.h)) {

          var confirmPopup = $ionicPopup.confirm({
            title: '¿Desea Finalizar el elemnto de iluminacion con su brazo?',
            template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para finalizar el registro del EI y seguir con Red.'
          })
          confirmPopup.then(function (res) {
            if (res) {
              //console.log("EI: " + JSON.stringify($scope.ei))
              //console.log("BRZ: " + JSON.stringify($scope.brazo))
              localBD.createEi($scope.ei).then(function (result) {
                if (result.insertId > 0) {
                  $state.go("redFarolTerra", {idActivo: $stateParams.idActivo}, {reload: true})
                } else {
                  $ionicPopup.alert({
                    title: '¡Ups hay un error!',
                    template: 'Hay un prblema interno con el dispositivo y los contraladores de almacenamiento, por favor reinicie la SALP e intentelo de nuevo.'
                  })
                  $ionicSlideBoxDelegate.slide(0, 500)
                  $scope.reiniciar()
                }
              }, function (error) {
                console.error(error);
              })
            } else {
              console.log('No desea continuar')
            }
          })

        } else {
          $ionicPopup.alert({
            title: '¡Ups hay un error!',
            template: 'Debe seleccionar los campos necesarios para finalizar y continuar'
          })
          $ionicSlideBoxDelegate.slide(0, 500)
          $scope.reiniciar()
        }

      }

      $scope.reiniciar = function () {
        $scope.ei.tipo = $stateParams.eiTipo
        $scope.ei.tecnologia = ''
        $scope.ei.v_tecnologia = false
        $scope.ei.nombre = ''
        $scope.ei.potencia = ''
        $scope.ei.v_potencia = false
        $scope.ei.foto = ''
        $scope.ei.observacion = ''
        $scope.ei.novedades = ''
        $scope.ei.serial = ''
        $scope.ei.apoyo_id = $stateParams.idApoyo
        $scope.ei.activo_id = 0
        $scope.pictureURl = ""


        $scope.tecnologia = {
          sodio: false,
          led: false,
          metal_halide: false,
          mercurio: false,
          luz_mixta: false
        }

        $scope.luminaria = {
          potenciaSodioA: false,
          potenciaSodioB: false,
          potenciaSodioC: false,
          potenciaSodioD: false,
          potenciaLedA: false,
          potenciaLedB: false,
          potenciaLedC: false,
          potenciaLedD: false,
          potenciaLedA20: false,
          potenciaLedA30: false,
          potenciaLedA32: false,
          potenciaLedA35: false,
          potenciaLedA36: false,
          potenciaLedA40: false,
          potenciaLedA42: false,
          potenciaLedB45: false,
          potenciaLedB46: false,
          potenciaLedB47: false,
          potenciaLedB50: false,
          potenciaLedB55: false,
          potenciaLedB60: false,
          potenciaLedB65: false,
          potenciaLedC70: false,
          potenciaLedC77: false,
          potenciaLedC90: false,
          potenciaLedD100: false,
          potenciaLedD120: false,
          potenciaMH70: false,
          potenciaMH120: false,
          potenciaMer125: false,
          potenciaMer250: false,
          potenciaMer400: false,
          potenciaLmx: false,
        }

        $scope.potenciaLed = ''

        $scope.farol = {
          potenciaSodio70: false,
          potenciaSodio150: false,
          potenciaSodio250: false,
          potenciaLed40: false,
          potenciaLed60: false,
        }

        $scope.reflector = {
          potenciaSodio: false
        }

        $scope.chk = {
          a: false,
          b: false,
          c: false,
          d: false,
          e: false,
          f: false,
          g: false
        }
      }

    }
  )
