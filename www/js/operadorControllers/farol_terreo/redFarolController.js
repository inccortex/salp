angular.module('starter.redFarolControllers', [])

  .controller("tipoRedFarolCtrl", function (userApi, localBD, $scope, $state, $stateParams, $ionicPopup, $ionicHistory, CONFIG, $ionicLoading, $http, $cordovaNetwork, $cordovaSQLite) {
    var loadData = function () {
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()

      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500,
      }

      $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
        // data.slider is the instance of Swiper
        $scope.slider = data.slider
      });

      $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
        //console.log('Slide change is beginning')
      });

      $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
        // note: the indexes are 0-based
        $scope.activeIndex = data.slider.activeIndex
        $scope.previousIndex = data.slider.previousIndex
      });

      $scope.red = {
        tipo: 0,
        v_tipo: 0,
        distancia: 0,
        v_distancia: 0,
        aerea: 0,
        observacion: '',
        novedades: '',
        activo_id: $stateParams.idActivo
      }

      $scope.redComp = {
        cens: false,
        exclu: false,
      }

      $scope.chk = {
        a: false,
        b: false,
        c: false
      }


    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    $scope.redCompf = function () {

      $scope.red.tipo = 0

      if ($scope.redComp.cens) {
        $scope.redComp.exclu = false
        $scope.red.tipo = 1
        $scope.red.aerea = 2
        $scope.red.distancia = 0
      }
      if ($scope.redComp.exclu) {
        $scope.redComp.cens = false
        $scope.red.tipo = 2
      }

    }

    $scope.finalRed = function () {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        userApi.isOpen().then(function (response) {
          if (response.data.status === "fail") {
            alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
            ionic.Platform.exitApp();
          }
        })
      }

      console.log("REF: " + JSON.stringify($scope.red))

      if ($scope.red.novedades !== '' && $scope.red.tipo > 0 && $scope.red.distancia >= 0 && $scope.red.aerea >= 0) {

        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea finalizar el Registro?',
          template: '¿Esta seguro@ que desea finalizar el registro?'
        })
        confirmPopup.then(function (res) {
          if (res) {
            var isOnline = $cordovaNetwork.isOnline()

            localBD.createRed($scope.red).then(function (resultB) {
              if (isOnline) {
                $ionicLoading.show({
                  template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
                });

                //TODO: ENVIAR PETICION AL SERVIDOR
                var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
                $scope.activo = {}
                var eiQ = "SELECT * FROM iluminacion WHERE iluminacion.activo_id = ? LIMIT 1"
                $scope.ei = {}
                var redQ = "SELECT * FROM red WHERE red.activo_id = ? LIMIT 1"
                $scope.red = {}

                var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
                var activoID = window.localStorage.getItem('activo_actual')
                //console.log("Activo actual - " + activoID)
                $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
                  if (res.rows.length > 0) {
                    $scope.activo = {
                      longitud: res.rows.item(0).longitud,
                      latitud: res.rows.item(0).latitud,
                      direccion: res.rows.item(0).direccion,
                      nomenclatura: res.rows.item(0).nomenclatura,
                      barrio: res.rows.item(0).barrio
                    }
                    $cordovaSQLite.execute(db, eiQ, [activoID]).then(function (resei) {
                      if (resei.rows.length > 0) {
                        //c/onsole.log("encuentra eis - ")
                        $scope.ei = {
                          tipo: resei.rows.item(0).tipo,
                          tecnologia: resei.rows.item(0).tecnologia,
                          v_tecnologia: resei.rows.item(0).v_tecnologia,
                          nombre: resei.rows.item(0).nombre,
                          potencia: resei.rows.item(0).potencia,
                          v_potencia: resei.rows.item(0).v_potencia,
                          foto: resei.rows.item(0).foto,
                          observacion: resei.rows.item(0).observacion,
                          novedades: resei.rows.item(0).novedades,
                          serial: resei.rows.item(0).serial
                        }
                        //console.log("SELECTED -> " + resei.rows.item(0).tipo + "  ");


                        $cordovaSQLite.execute(db, redQ, [activoID]).then(function (resred) {
                          if (resred.rows.length > 0) {
                            $scope.red = {
                              tipo: resred.rows.item(0).tipo,
                              v_tipo: resred.rows.item(0).v_tipo,
                              distancia: resred.rows.item(0).distancia,
                              v_distancia: resred.rows.item(0).v_distancia,
                              aerea: resred.rows.item(0).aerea,
                              observacion: resred.rows.item(0).observacion,
                              novedades: resred.rows.item(0).novedades
                            }

                            //console.log("ACTIVO: " + JSON.stringify($scope.activo))
                            //console.log("EI: " + $scope.ei)
                            //console.log("RED: " + JSON.stringify($scope.red))
                            var user_login = JSON.parse(window.localStorage.getItem('usuario'))
                            var ruta = ""
                            if ($scope.ei.tipo === 2) {
                              ruta = "guardarFarol"
                            } else if ($scope.ei.tipo === 4) {
                              ruta = "guardarTerreo"
                            }

                            var Digital = new Date()
                            var hours = Digital.getHours()
                            var minutes = Digital.getMinutes()
                            var seconds = Digital.getSeconds()

                            var hoy = new Date();
                            var dd = hoy.getDate();
                            var mm = hoy.getMonth() + 1;
                            var yyyy = hoy.getFullYear();
                            console.log('fecha_registro:' + yyyy + "-" + mm + "-" + dd + " --- " + 'hora:' + hours + ":" + minutes + " " + seconds)

                            $http({
                              method: 'POST',
                              url: CONFIG.APIURL + 'api/' + ruta,
                              data: {
                                activo: {
                                  latitud: $scope.activo.latitud,
                                  longitud: $scope.activo.longitud,
                                  fecha_registro: yyyy + "-" + mm + "-" + dd,
                                  hora: hours + ":" + minutes + ":" + seconds,
                                  direccion: $scope.activo.direccion,
                                  nomenclatura: $scope.activo.nomenclatura,
                                  barrio: $scope.activo.barrio,
                                  comuna: $scope.activo.comuna
                                },
                                apoyo: $scope.apoyo,
                                EI: $scope.ei,
                                red: $scope.red,
                                brazo: $scope.brz,
                                token: user_login.token,
                                users_id: user_login.usuario.id
                              },
                            }).then(function successCallback(response) {
                              //console.log(JSON.stringify(response))
                              $ionicLoading.hide();
                              $ionicPopup.alert({
                                title: '¡Felicidades!',
                                template: 'Se ha registrado el activo exitosamente.'
                              });
                              window.localStorage.removeItem('activo_actual')
                              window.localStorage.removeItem('reg_actual')
                              $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                              $cordovaSQLite.execute(db, "DELETE FROM iluminacion WHERE iluminacion.activo_id = ?", [activoID])
                              $cordovaSQLite.execute(db, "DELETE FROM red WHERE red.activo_id = ?", [activoID])
                              $state.go("profileOperador", {}, {reload: true})
                            }, function errorCallback(response) {
                              $ionicPopup.alert({
                                title: '¡Ups, parece que hay un error!',
                                template: 'Se ha persentado un inconveniente al conectar con el servidor, los datos quedaran almacenados en el dispositivo, por favor intentelo de nuevo mas tarde..'
                              });
                              $ionicLoading.hide();
                              window.localStorage.removeItem('activo_actual')
                              window.localStorage.removeItem('reg_actual')
                              $state.go("profileOperador", {}, {reload: true})
                            });
                          }
                        })
                      }
                    }), function (err) {
                      console.error(err);
                    }
                  }
                }, function (err) {
                  console.error(err);
                })


              } else {
                $ionicPopup.alert({
                  title: '¡Ups, parece que no hay Conexion!',
                  template: 'No se ha detectado conexion a la red, los datos registrados se alamcenarán localmente.'
                });
                window.localStorage.removeItem('activo_actual')
                window.localStorage.removeItem('reg_actual')
                $state.go("profileOperador", {}, {reload: true})
              }
            })


          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe digitar los campos necesarios para finalizar y continuar'
        })
      }

    }
  })
