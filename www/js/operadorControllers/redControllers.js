angular.module('starter.redControllers', [])

  .controller("regisroRedCtrl", function (userApi, CONFIG, $cordovaSQLite, $scope, $state, $stateParams, $ionicHistory, $cordovaNetwork, localBD, $http, $ionicLoading, $ionicPopup) {
    var loadData = function () {
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()

      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500,
      }

      $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
        // data.slider is the instance of Swiper
        $scope.slider = data.slider
      });

      $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
        //console.log('Slide change is beginning')
      });

      $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
        // note: the indexes are 0-based
        $scope.activeIndex = data.slider.activeIndex
        $scope.previousIndex = data.slider.previousIndex
      });

      $scope.red = {
        tipo: 0,
        v_tipo: 0,
        distancia: 0,
        v_distancia: 0,
        aerea: 0,
        observacion: '',
        novedades: '',
        activo_id: $stateParams.idActivo
      }

      $scope.redComp = {
        cens: false,
        exclu: false,
      }

      $scope.chk = {
        a: false,
        b: false,
        c: false
      }


    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    $scope.redCompf = function () {

      $scope.red.tipo = 0

      if ($scope.redComp.cens) {
        $scope.redComp.exclu = false
        $scope.red.tipo = 1
        $scope.red.aerea = 2
        $scope.red.distancia = 0
      }
      if ($scope.redComp.exclu) {
        $scope.redComp.cens = false
        $scope.red.tipo = 2
      }

    }

    $scope.finalRed = function () {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        userApi.isOpen().then(function (response) {
          if (response.data.status === "fail") {
            alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
            ionic.Platform.exitApp();
          }
        })
      }

      console.log("REF: " + JSON.stringify($scope.red))

      if ($scope.red.novedades !== '' && $scope.red.tipo > 0 && $scope.red.distancia >= 0 && $scope.red.aerea >= 0) {

        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea finalizar el Registro?',
          template: '¿Esta seguro@ que desea finalizar el registro?'
        })
        confirmPopup.then(function (res) {
          if (res) {
            var isOnline = $cordovaNetwork.isOnline()

            localBD.createRed($scope.red).then(function (resultB) {
              if (isOnline) {
                $ionicLoading.show({
                  template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
                });

                var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
                $scope.activo = {}
                var apoyoQ = "SELECT * FROM apoyo WHERE apoyo.activo_id = ? LIMIT 1"
                $scope.apoyo = {}
                var eiQ = "SELECT i.tipo as eiTipo, i.tecnologia, i.v_tecnologia, i.nombre, i.potencia, i.v_potencia, i.foto, i.observacion as eiObs, i.novedades as eiNov, i.serial, b.tipo as brzTipo, b.incorporado, b.observacion as brzObs, b.novedades as brzNov FROM iluminacion i JOIN brazo b ON b.ei_id = i.id WHERE i.apoyo_id = ?"
                $scope.ei = []
                var brzQ = "SELECT * FROM brazo WHERE brazo.ei_id = ? LIMIT 1"
                $scope.brz = {}
                var redQ = "SELECT * FROM red WHERE red.activo_id = ? LIMIT 1"
                $scope.red = {}


                var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
                var activoID = window.localStorage.getItem('activo_actual')
                var apId = 0
                console.log("Activo actual - " + activoID)
                $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
                  if (res.rows.length > 0) {
                    console.log("encuentra activo - ")
                    $scope.activo = {
                      longitud: res.rows.item(0).longitud,
                      latitud: res.rows.item(0).latitud,
                      direccion: res.rows.item(0).direccion,
                      nomenclatura: res.rows.item(0).nomenclatura,
                      barrio: res.rows.item(0).barrio
                    }
                    $cordovaSQLite.execute(db, apoyoQ, [activoID]).then(function (resAP) {
                      if (resAP.rows.length > 0) {
                        apId = resAP.rows.item(0).id
                        console.log("encuentra apoyo - ")
                        $scope.apoyo = {
                          pertenece: resAP.rows.item(0).pertenece,
                          v_pertenece: resAP.rows.item(0).v_pertenece,
                          tipo: resAP.rows.item(0).tipo,
                          v_tipo: resAP.rows.item(0).v_tipo,
                          longitud: resAP.rows.item(0).longitud,
                          v_longitud: resAP.rows.item(0).v_longitud,
                          observacion: resAP.rows.item(0).observacion,
                          novedades: resAP.rows.item(0).novedades,
                        }

                        $cordovaSQLite.execute(db, eiQ, [resAP.rows.item(0).id]).then(function (resEi) {
                          if (resEi.rows.length > 0) {
                            var i = 0
                            for (i = 0; i < resEi.rows.length; i++) {
                              console.log("CONTEO I: " + i)

                              $scope.ei.push(
                                [{
                                  tipo: resEi.rows.item(i).eiTipo,
                                  tecnologia: resEi.rows.item(i).tecnologia,
                                  v_tecnologia: resEi.rows.item(i).v_tecnologia,
                                  nombre: resEi.rows.item(i).nombre,
                                  potencia: resEi.rows.item(i).potencia,
                                  v_potencia: resEi.rows.item(i).v_potencia,
                                  foto: resEi.rows.item(i).foto,
                                  observacion: resEi.rows.item(i).eiObs,
                                  novedades: resEi.rows.item(i).eiNov,
                                  serial: resEi.rows.item(i).serial
                                }, {
                                  tipo: resEi.rows.item(i).brzTipo,
                                  incorporado: resEi.rows.item(i).incorporado,
                                  observaciones: resEi.rows.item(i).brzObs,
                                  novedades: resEi.rows.item(i).brzNov
                                }])
                            }

                            $cordovaSQLite.execute(db, redQ, [activoID]).then(function (resRed) {
                              if (resRed.rows.length > 0) {
                                ("encuentra red - ")
                                $scope.red = {
                                  tipo: resRed.rows.item(0).tipo,
                                  v_tipo: resRed.rows.item(0).v_tipo,
                                  distancia: resRed.rows.item(0).distancia,
                                  v_distancia: resRed.rows.item(0).v_distancia,
                                  aerea: resRed.rows.item(0).aerea,
                                  observacion: resRed.rows.item(0).observacion,
                                  novedades: resRed.rows.item(0).novedades
                                }

                                console.log("ACTIVO: " + JSON.stringify($scope.activo))
                                /*console.log("APOYO: " + JSON.stringify($scope.apoyo))
                                 console.log("EI: " + $scope.ei)
                                 console.log("RED: " + JSON.stringify($scope.red))*/

                                var user_login = JSON.parse(window.localStorage.getItem('usuario'))

                                var Digital = new Date()
                                var hours = Digital.getHours()
                                var minutes = Digital.getMinutes()
                                var seconds = Digital.getSeconds()

                                var hoy = new Date();
                                var dd = hoy.getDate();
                                var mm = hoy.getMonth() + 1;
                                var yyyy = hoy.getFullYear();
                                console.log('fecha_registro:' + yyyy + "-" + mm + "-" + dd + " --- " + 'hora:' + hours + ":" + minutes + " " + seconds)

                                $http({
                                  method: 'POST',
                                  url: CONFIG.APIURL + 'api/guardarLuminariaReflector',
                                  data: {
                                    activo: {
                                      latitud: $scope.activo.latitud,
                                      longitud: $scope.activo.longitud,
                                      fecha_registro: yyyy + "-" + mm + "-" + dd,
                                      hora: hours + ":" + minutes + ":" + seconds,
                                      direccion: $scope.activo.direccion,
                                      nomenclatura: $scope.activo.nomenclatura,
                                      barrio: $scope.activo.barrio,
                                      comuna: $scope.activo.comuna
                                    },
                                    apoyo: $scope.apoyo,
                                    EI: $scope.ei,
                                    red: $scope.red,
                                    token: user_login.token,
                                    users_id: user_login.usuario.id
                                  },
                                }).then(function successCallback(response) {
                                  //console.log(JSON.stringify(response))
                                  $ionicLoading.hide();
                                  $ionicPopup.alert({
                                    title: '¡Felicidades!',
                                    template: 'Se registrado el activo exitosamente..'
                                  });
                                  window.localStorage.removeItem('activo_actual')
                                  window.localStorage.removeItem('reg_actual')
                                  $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                                  $cordovaSQLite.execute(db, "DELETE FROM apoyo WHERE apoyo.activo_id = ?", [activoID])
                                  $cordovaSQLite.execute(db, "DELETE FROM brazo WHERE brazo.activo_id = ?", [activoID])
                                  $cordovaSQLite.execute(db, "DELETE FROM iluminacion WHERE iluminacion.apoyo_id = ?", [apId])
                                  $cordovaSQLite.execute(db, "DELETE FROM red WHERE red.activo_id = ?", [activoID])
                                  $state.go("profileOperador", {}, {reload: true})
                                }, function errorCallback(response) {
                                  //console.log(JSON.stringify(response))
                                  $ionicPopup.alert({
                                    title: '¡Ups, no se puede conectar con el servidor!',
                                    template: 'No se ha detectado conexion con el servidor, los datos registrados se alamcenarán localmente.'
                                  });
                                  window.localStorage.removeItem('activo_actual')
                                  window.localStorage.removeItem('reg_actual')
                                  $state.go("profileOperador", {}, {reload: true})
                                  $ionicLoading.hide();
                                });

                              }
                            }, function (err) {
                              console.error(err);
                            })

                          }
                        }, function (err) {
                          console.error(err);
                        })

                      }
                    }, function (err) {
                      console.error(err);
                    })
                  }
                }, function (err) {
                  console.error(err);
                })

              } else {
                $ionicPopup.alert({
                  title: '¡Ups, parece que no hay Conexion!',
                  template: 'No se ha detectado conexion a la red, los datos registrados se alamcenarán localmente.'
                });
                window.localStorage.removeItem('activo_actual')
                window.localStorage.removeItem('reg_actual')
                $state.go("profileOperador", {}, {reload: true})
              }
            })


          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe digitar los campos necesarios para finalizar y continuar'
        })
      }

    }

  })
