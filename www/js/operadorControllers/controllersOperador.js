angular.module('starter.controllersOperador', [])

  .controller("ProfileOperadorCtrl", function ($cordovaNetwork, $scope, $state, CONFIG, $ionicPopup, $ionicHistory, $http, $cordovaSQLite, userApi) {

    var loadData = function () {
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()
      $ionicHistory.nextViewOptions({
        disableBack: true
      });

      var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
      var activoID = window.localStorage.getItem('activo_actual')
      if (activoID) {
        $cordovaSQLite.execute(db, "DELETE FROM 'activo' WHERE activo.id = ?", [activoID])
        window.localStorage.removeItem('activo_actual')
      }


    }

    $scope.manual = function(){
      $state.go("manual", {'num': '1'})
    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    //dirige a la vista de la información del app
    $scope.toInfo = function () {
      $state.go("info")
    }

    $scope.toEstadisticas = function () {
      $state.go("estadisticas")
    }

    $scope.toRegistros = function () {
      $state.go("registros")
    }

    $scope.toRegLocales = function () {
      $state.go("registrosLocales")
    }

    //dirige a realizar un nuevo registro
    $scope.toNewRegister = function () {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        userApi.isOpen().then(function (response) {
          if (response.data.status === "fail") {
            alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
            ionic.Platform.exitApp();
          }
        })
      }

      $state.go("newRegisterOperador")
    }

    var user = JSON.parse(window.localStorage.getItem('usuario'))
    $scope.imageProfile = CONFIG.APIURL2 + user.usuario.profile_image
    $scope.dataActivo = window.localStorage.getItem('activo_actual')
    $scope.activo = {id: undefined}
    /*if ($scope.dataActivo) {
     $ionicPopup.alert({
     title: '¡Parece que tienes un registro pendiente por terminar!',
     template: 'Debes terminar el registro del activo completo para realizar un nuevo registro <br/><br/>'
     })
     $scope.activo.id = $scope.dataActivo
     }*/

    /*$scope.toContinue = function () {
     var type = window.localStorage.getItem('reg_actual')
     if (type === "A") {
     $ionicHistory.clearCache()
     $ionicHistory.clearHistory()
     $state.go('eiTipoEI', {'idActivo': $scope.activo.id}, {reload: true})
     } else {
     $state.go('trafoPotencia', {'idActivo': $scope.activo.id}, {reload: true})
     }
     }*/

  })

  .controller("NewRegisterOperadorOperadorCtrl", function (userApi, localBD, $ionicHistory, $http, $scope, $state, $cordovaCamera, $cordovaGeolocation, $ionicLoading, $cordovaNetwork, CONFIG, $ionicPopup, $cordovaSQLite, $http) {


    var user_login = JSON.parse(window.localStorage.getItem('usuario'))
    //Base de activos
    $scope.token = user_login.token
    $scope.data = {type: null}
    $scope.activo = {id: undefined}


    $scope.dataActivo = {
      latitud: "",
      longitud: "",
      users_id: user_login.usuario.id,
      direccion: "",
      nomenclatura: "",
      barrio: "",
      comuna: ""
    }
    $scope.direct = true
    $scope.barrio = ""

    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    $scope.states = ["CALLEJON, COMUNA 1", "CENTRO, COMUNA 1", "LA PLAYA, COMUNA 1", "LA SEXTA, COMUNA 1", "LATINO, COMUNA 1", "PARAMO, COMUNA 1", "LLANO, COMUNA 1", "ACUARELA, COMUNA 2", "BALCONES DE SAN FRANCISCO, COMUNA 2", "BLANCO, COMUNA 2", "BRISAS DEL PAMPLONITA, COMUNA 2", "CAOBOS, COMUNA 2", "CEIBA I, COMUNA 2", "CEIBA II, COMUNA 2", "COLSAG, COMUNA 2", "COMERCIAL BOLIVAR, COMUNA 2", "CONDADO DE CASTILLA, COMUNA 2", "EL ROSAL, COMUNA 2", "GALICIA, COMUNA 2", "GOVIKA, COMUNA 2", "HACARITAMA, COMUNA 2", "HURAPANES, COMUNA 2", "LA CAPILLANA, COMUNA 2", "LA CASTELLANA, COMUNA 2", "LA CEIBA, COMUNA 2", "LA ESPERANZA, COMUNA 2", "LA PRIMAVERA, COMUNA 2", "LA RINCONADA, COMUNA 2", "LA RIVIERA, COMUNA 2", "LAS ALMEYDAS, COMUNA 2", "LIBERTADORES, COMUNA 2", "LOS ACACIOS, COMUNA 2", "LOS PINOS, COMUNA 2", "LOS PROCERES, COMUNA 2", "MANOLO LEMUS, COMUNA 2", "MIRADOR CAMPESTRE, COMUNA 2", "PALMA REAL, COMUNA 2", "PARQUE REAL, COMUNA 2", "PARQUES RESIDENCIALES LOS LIBERTADORES, COMUNA 2", "POPULAR, COMUNA 2", "PRADOS CLUB, COMUNA 2", "PRADOS I, COMUNA 2", "PRADOS II, COMUNA 2", "QUINTA BOSCH, COMUNA 2", "QUINTA ORIENTAL, COMUNA 2", "QUINTA VELEZ, COMUNA 2", "RINCON DE LOS PRADOS, COMUNA 2", "SAN ISIDRO, COMUNA 2", "SAN REMO, COMUNA 2", "SANTA LUCIA, COMUNA 2", "SAYAGO, COMUNA 2", "THE RIVERS COUNTRY, COMUNA 2", "VALPARAISO SUIT, COMUNA 2", "VILLA MARIA, COMUNA 2", "AGUAS CALIENTES, COMUNA 3", "BELLAVISTA, COMUNA 3", "BETHEL, COMUNA 3", "BOCONO, COMUNA 3", "BOGOTA, COMUNA 3", "LA CAROLINA, COMUNA 3", "LA LIBERTAD, COMUNA 3", "LA UNION, COMUNA 3", "LAS MARGARITAS, COMUNA 3", "LUIS PEREZ HERNANDEZ, COMUNA 3", "MORELLY, COMUNA 3", "POLICARPA, COMUNA 3", "SAN MATEO, COMUNA 3", "SANTA ANA, COMUNA 3", "URBANIZACION SANTA ANA, COMUNA 3", "VALLE ESTHER, COMUNA 3", "13 DE MARZO, COMUNA 4", "ALTO PAMPLONITA, COMUNA 4", "ANIVERSARIO I, COMUNA 4", "ANIVERSARIO II, COMUNA 4", "BAJO PAMPLONITA, COMUNA 4", "BOSQUES DEL PAMPLONITA, COMUNA 4", "CAÑAFISTOLO, COMUNA 4", "EL HIGUERON, COMUNA 4", "ESTACION DEL ESTE, COMUNA 4", "HELIOPOLIS, COMUNA 4", "LA ALAMEDA, COMUNA 4", "LA CAMPIÑA, COMUNA 4", "LA FLORIDA, COMUNA 4", "LA ISLA, COMUNA 4", "LA QUINTA, COMUNA 4", "NUEVA SANTA CLARA, COMUNA 4", "NUEVO ESCOBAL, COMUNA 4", "PORTAL DEL ESCOBAL, COMUNA 4", "PRADOS DEL ESTE, COMUNA 4", "CONJUNTO PRADOSDEL ESTE, COMUNA 4", "SAN JOSE, COMUNA 4", "SAN LUIS, COMUNA 4", "SAN MARTIN I, COMUNA 4", "SAN MARTIN II, COMUNA 4", "SANTA TERESITA, COMUNA 4", "SANTILLANA, COMUNA 4", "TERRANOVA, COMUNA 4", "TORCOROMA, COMUNA 4", "TORCOROMA SIGLO XXI, COMUNA 4", "VIEJO ESCOBAL, COMUNA 4", "VILLA CAMILA, COMUNA 4", "VILLAS DE SAN DIEGO, COMUNA 4", "ALCALA, COMUNA 5", "CIUDAD JARDIN, COMUNA 5", "COLPET, COMUNA 5", "EL BOSQUE, COMUNA 5", "GRATAMIRA, COMUNA 5", "GUAIMARAL, COMUNA 5", "GUALANDAY, COMUNA 5", "JUANA RANGEL DE CUELLAR, COMUNA 5", "LA MAR, COMUNA 5", "LA MARIA, COMUNA 5", "LA MERCED, COMUNA 5", "LINARES, COMUNA 5", "LLERAS RESTREPO, COMUNA 5", "LOS ANGELES, COMUNA 5", "NIZA, COMUNA 5", "PARAISO, COMUNA 5", "PESCADERO, COMUNA 5", "PORTACHUELO, COMUNA 5", "PRADOS DEL NORTE, COMUNA 5", "SAN EDUARDO I Y II, COMUNA 5", "SANTAHELENA, COMUNA 5", "SEVILLA, COMUNA 5", "TASAJERO, COMUNA 5", "ZULIMA I ETAPA, COMUNA 5", "ZULIMA II ETAPA, COMUNA 5", "ZULIMA III ETAPA, COMUNA 5", "ZULIMA IV ETAPA, COMUNA 5", "20 DE DICIEMBRE, COMUNA 6", "6 DE MAYO, COMUNA 6", "8 DE DICIEMBRE, COMUNA 6", "AEROPUERTO, COMUNA 6", "ALONSITO, COMUNA 6", "BRISAS DEL AEROPUERTO, COMUNA 6", "BRISAS DEL NORTE, COMUNA 6", "BRISAS DEL PARAISO, COMUNA 6", "CAMILO DAZA, COMUNA 6", "CAÑO LIMON, COMUNA 6", "CARLOSGARCIA LOZADA, COMUNA 6", "CARLOS PIZARRO, COMUNA 6", "CECILIA CASTRO, COMUNA 6", "CERRO DE LA CRUZ, COMUNA 6", "CERRO NORTE, COMUNA 6", "COLINAS DE LA VICTORIA, COMUNA 6", "COLINAS DEL SALADO, COMUNA 6", "CONJ. CERRADO MOLINOS DEL NORTE, COMUNA 6", "CUMBRES DEL NORTE, COMUNA 6", "DIVINO NIÑO, COMUNA 6", "EL CERRO, COMUNA 6", "EL SALADO, COMUNA 6", "ESPERANZA MARTINEZ, COMUNA 6", "GARCIA HERREROS I, COMUNA 6", "GARCIA HERREROS II, COMUNA 6", "LA CONCORDIA, COMUNA 6", "LA INSULA, COMUNA 6", "LIMONAR DEL NORTE, COMUNA 6", "LOS LAURELES, COMUNA 6", "MARIA AUXILIADORA, COMUNA 6", "MARIA PAZ, COMUNA 6", "MOLINOS DEL NORTE, COMUNA 6", "NUEVA COLOMBIA, COMUNA 6", "OLGA TERESA, COMUNA 6", "PANAMERICANO, COMUNA 6", "PORVENIR, COMUNA 6", "RAFAEL NUÑEZ, COMUNA 6", "SAN GERARDO, COMUNA 6", "SIMON BOLIVAR I, COMUNA 6", "TOLEDO PLATA, COMUNA 6", "TRIGAL DEL NORTE, COMUNA 6", "URBANIZACION LAS AMERICAS, COMUNA 6", "VILLA JULIANA, COMUNA 6", "VILLA NUEVA, COMUNA 6", "VILLAS DEL TEJAR, COMUNA 6", "VIRGILIO BARCO, COMUNA 6", "BUENOS AIRES, COMUNA 7", "CHAPINERO, COMUNA 7", "CLARET, COMUNA 7", "COLOMBIA I, COMUNA 7", "COLOMBIA II, COMUNA 7", "COMUNEROS, COMUNA 7", "EL PARAISO, COMUNA 7", "EL ROSAL DEL NORTE, COMUNA 7", "LA FLORIDA, COMUNA 7", "LA HERMITA, COMUNA 7", "LA LAGUNA, COMUNA 7", "LA PRIMAVERA, COMUNA 7", "LAS AMERICAS, COMUNA 7", "MOTILONES, COMUNA 7", "OSPINA PEREZ, COMUNA 7", "TUCUNARE, COMUNA 7", "6 DE ENERO, COMUNA 8", "7 DE AGOSTO, COMUNA 8", "ANTONIA SANTOS, COMUNA 8", "ATALAYA, COMUNA 8", "ATALAYA I ETAPA, COMUNA 8", "ATALAYA II ETAPA, COMUNA 8", "ATALAYA III ETAPA, COMUNA 8", "BELISARIO, COMUNA 8", "CARLOS RAMIREZ PARIS, COMUNA 8", "CERROPICO, COMUNA 8", "CUCUTA 75, COMUNA 8", "DOÑA NIDIA, COMUNA 8", "EL DESIERTO, COMUNA 8", "EL MINUTO DE DIOS, COMUNA 8", "EL PROGRESO, COMUNA 8", "EL RODEO, COMUNA 8", "JUAN RANGEL, COMUNA 8", "LA CAROLINA, COMUNA 8", "LA VICTORIA, COMUNA 8", "LOS ALMENDROS, COMUNA 8", "LOS OLIVOS, COMUNA 8", "NIÑA CECI, COMUNA 8", "NUEVO HORIZONTE, COMUNA 8", "PALMERAS, COMUNA 8", "VALLES DEL RODEO, COMUNA 8", "28 DEFEBRERO, COMUNA 9", "AISLANDIA, COMUNA 9", "ARNULFO BRICEÑO, COMUNA 9", "BARRIO NUEVO, COMUNA 9", "BELEN, COMUNA 9", "BELEN DE UMBRIA, COMUNA 9", "CARORA, COMUNA 9", "CUNDINAMARCA, COMUNA 9", "DIVINA PASTORA, COMUNA 9", "EL REPOSO, COMUNA 9", "FATIMA, COMUNA 9", "LAS COLINAS, COMUNA 9", "LOMA DE BOLIVAR, COMUNA 9", "LOS ALPES, COMUNA 9", "PUEBLO NUEVO, COMUNA 9", "RUDESINDO SOTO, COMUNA 9", "SAN MIGUEL, COMUNA 9", "ALFONSO LOPEZ, COMUNA 10", "CAMILO TORRES, COMUNA 10", "CIRCUNVALACION, COMUNA 10", "CUBEROS NIÑO, COMUNA 10", "EL RESUMEN, COMUNA 10", "GAITAN, COMUNA 10", "GALAN, COMUNA 10", "LA AURORA, COMUNA 10", "LA CABRERA, COMUNA 10", "LAS MALVINAS, COMUNA 10", "MAGDALENA, COMUNA 10", "PUENTE BARCO, COMUNA 10", "SAN JOSE, COMUNA 10", "SAN RAFAEL, COMUNA 10", "SANTANDER, COMUNA 10", "SANTO DOMINGO, COMUNA 10"]

    var loadData = function () {
      $scope.activo.id = undefined
      $scope.activo_actual = JSON.parse(window.localStorage.getItem('activo_actual'))
      if ($scope.activo_actual) {
        $scope.activo.id = $scope.activo_actual
      }
    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    $scope.toContinue = function () {

      var type = window.localStorage.getItem('reg_actual')
      if (type === "A") {
        $state.go('eiTipoEI')
      } else {
        $state.go('registroTrafo')
      }

    }

    //cancelamos el nuevo registro
    $scope.cancel = function () {
      $state.go("profileOperador")
    }

    /**
     * @function - toma la geolocalizacion latitude y longitde
     */
    $scope.tomarGeolocacion = function () {
      //Petición al servidor para realizar el login
      $ionicLoading.show({
        template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
      });
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
        .then(function (position) {
          $scope.dataActivo.latitud = position.coords.latitude
          $scope.dataActivo.longitud = position.coords.longitude
          $ionicLoading.hide()
        }, function (err) {
          //console.log(err)
          $ionicPopup.alert({
            title: '¡Ups, hay un error!',
            template: 'Debe encender el gps del celular.'
          });
          $ionicLoading.hide()
        });

    }

    /**
     * @function - almacena un nuevo activo en localstorage y verifica si hay internet, lo envia al servidor
     */
    $scope.newActivo = function () {

      //console.log("TOKEN - " + $scope.dataActivo.token)
      //console.log("USER - " + $scope.dataActivo.users_id)
      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        userApi.isOpen().then(function (response) {
          console.log("Save activo - OpenSystem: " + JSON.stringify(response))
          if (response.data.status === "fail") {
            alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
            ionic.Platform.exitApp();
            return
          }
        })
      }

      //validaciones para ver que esten llenos los datos del formulario
      var validate = 0
      if ($scope.dataActivo.latitud === "" || $scope.dataActivo.latitud === undefined) {
        validate++
      }
      if ($scope.dataActivo.direccion === "" || $scope.dataActivo.direccion === undefined) {
        validate++
      }
      if ($scope.dataActivo.barrio === "" || $scope.dataActivo.barrio === undefined) {
        validate++
      }
      if ($scope.dataActivo.nomenclatura === "" || $scope.dataActivo.nomenclatura === undefined) {
        validate++
      }
      if ($scope.dataActivo.longitud === "" || $scope.dataActivo.longitud === undefined) {
        validate++
      }
      if ($scope.data.type === "x" || $scope.data.type === undefined || $scope.data.type === "") {
        validate++
      }

      //si estan llenos los campos, se continua el registro
      if (validate === 0) {
        $ionicLoading.show({
          template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
        })

        //GUARDO EL REGISTRO DEL NUEVO ACTIVO EN LA BD

        var cb = $scope.dataActivo.barrio.split(",")
        $scope.dataActivo.comuna = cb[1]
        $scope.dataActivo.barrio = cb[0]
        console.log(JSON.stringify($scope.dataActivo))
        localBD.createActivo($scope.dataActivo).then(function (result) {
          $ionicLoading.hide()

          if (result.insertId > 0) {
            $ionicLoading.hide();
            window.localStorage.setItem('reg_actual', $scope.data.type)
            window.localStorage.setItem('activo_actual', result.insertId)

            //REDIRECCIONO A LA VISTA SEGUN CORRESPONDA
            if ($scope.data.type === 1 || $scope.data.type === 3) {
              $state.go('registroApoyo_1', {'idActivo': result.insertId, 'eiTipo': $scope.data.type}, {reload: true})
            } else if ($scope.data.type === 2 || $scope.data.type === 4) {
              $state.go('eiFarolTerra', {'idActivo': result.insertId, 'eiTipo': $scope.data.type}, {reload: true})
            } else if ($scope.data.type === 5) {
              $state.go('trafoPotencia', {'idActivo': result.insertId}, {reload: true})
            }

          } else {
            console.log("No hace una mierda")
          }

        }, function (error) {
          console.error(error);
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups, hay un error!',
          template: 'Debe registrar todos los datos para continuar'
        })
      }
    }


  })


  //INICIO CTRLS REGISTROS
  .controller("registrosCtrl", function ($scope, $state, $ionicPopup, $cordovaSQLite) {

  })

  .controller("estadisticasCtrl", function ($scope, $state, $ionicPopup, $http, CONFIG, $ionicHistory, $ionicLoading, $cordovaNetwork) {

    $ionicHistory.clearCache()
    $ionicHistory.clearHistory()

    $scope.data = {
      meta_hoy: '',
      realizado: '',
      meta_global: '',
      historico: '',
      ponderado: '',
    }

    $scope.ind = 0

    $scope.back = function () {
      $state.go("profileOperador", {}, {reload: true})
    }

    $scope.info = function () {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        $ionicLoading.show({
          template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
        });

        var user = JSON.parse(window.localStorage.getItem('usuario'))

        $http({
          method: 'POST',
          url: CONFIG.APIURL + 'api/indicadoresOperador',
          data: {
            'token': user.token,
            'users_id': user.usuario.id
          },
        }).then(function successCallback(response) {
          //console.log(JSON.stringify(response.data.status))
          if (response.data.status === "ok") {
            $scope.ind = 1
            $scope.data.meta_global = response.data.msj.meta_global
            $scope.data.realizado = response.data.msj.realizado
            $scope.data.meta_hoy = response.data.msj.meta_hoy
            $scope.data.historico = response.data.msj.historico
            $scope.data.ponderado = JSON.stringify(response.data.msj.metaPonderado)
          } else {

          }
          $ionicLoading.hide();
        }, function errorCallback(response) {
          console.log(JSON.stringify(response))
          $ionicPopup.alert({
            title: '¡Ups, hay un error al intentar conectar con el servidor!',
            template: 'Verifica la información e intentelo de nuevo, si el problema persiste comuniquelo al administrador de la plataforma.'
          })
          $state.go("profileOperador", {}, {reload: true})
          $ionicLoading.hide();
        });
      } else {
        $ionicPopup.alert({
          title: '¡Parece que no tienes conexion!',
          template: 'Debes conectarte a internet para realizar la actualización d ela informacion.'
        })
        $state.go("profileOperador", {}, {reload: true})
      }

    }

  })

  .controller("registrosLocalesCtrl", function (CONFIG, $cordovaNetwork, $scope, $state, $ionicPopup, $cordovaSQLite, $ionicLoading, $ionicHistory, $http) {

    $scope.data = []
    $scope.data2 = []
    $scope.data3 = []
    $scope.regTam = 0
    $scope.regTam2 = 0
    $scope.regTam3 = 0

    var loadData = function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();

      $scope.data = []
      $scope.data2 = []
      $scope.data3 = []
      $scope.regTam = 0
      $scope.regTam2 = 0
      $scope.regTam3 = 0

      $ionicLoading.show({
        template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
      });

      var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)

      var queryIn = "SELECT activo.id as idActivo, activo.latitud, activo.longitud, activo.barrio, activo.direccion, (SELECT iluminacion.tipo FROM iluminacion WHERE iluminacion.apoyo_id = apoyo.id LIMIT 1) as tipo FROM activo JOIN apoyo ON apoyo.activo_id = activo.id WHERE activo.finalizado LIKE 'NO'"
      var queryIn2 = "SELECT activo.id as idActivo,activo.direccion, activo.barrio, activo.latitud, activo.longitud, iluminacion.tipo FROM activo JOIN iluminacion ON iluminacion.activo_id = activo.id WHERE activo.finalizado LIKE 'NO'"
      var queryIn3 = "SELECT activo.id as idActivo,activo.direccion, activo.barrio, activo.latitud, activo.longitud FROM activo JOIN trafo ON trafo.activo_id = activo.id WHERE activo.finalizado LIKE 'NO'"
      //console.log("VER 1")
      $cordovaSQLite.execute(db, queryIn).then(function (res) {
        if (res.rows.length > 0) {
          $scope.regTam = res.rows.length
          for (var i = 0; i < res.rows.length; i++) {
            $scope.data.push(
              {
                idActivo: res.rows.item(i).idActivo,
                tipo: res.rows.item(i).tipo,
                latitud: res.rows.item(i).latitud,
                longitud: res.rows.item(i).longitud,
                barrio: res.rows.item(i).barrio,
                direccion: res.rows.item(i).direccion,
                comuna: res.rows.item(i).comuna
              })
            //console.log("SELECTED -> " + res.rows.item(i).eiTipo + "  ");
          }
        } else {
          console.log("No results found");
        }
        $ionicLoading.hide();
      }, function (err) {
        $ionicLoading.hide();
        console.error(err);
      });

      //console.log("VER 2")
      $cordovaSQLite.execute(db, queryIn2).then(function (res) {
        if (res.rows.length > 0) {
          $scope.regTam2 = res.rows.length
          for (var i = 0; i < res.rows.length; i++) {
            $scope.data2.push(
              {
                idActivo: res.rows.item(i).idActivo,
                tipo: res.rows.item(i).tipo,
                latitud: res.rows.item(i).latitud,
                longitud: res.rows.item(i).longitud,
                direccion: res.rows.item(i).direccion,
                barrio: res.rows.item(i).barrio
              })
            //console.log("SELECTED -> " + res.rows.item(i).eiTipo + "  ");
          }
        } else {
          console.log("No results found");
        }
        $ionicLoading.hide();
      }, function (err) {
        $ionicLoading.hide();
        console.error(err);
      });

      //console.log("VER 3")
      $cordovaSQLite.execute(db, queryIn3).then(function (res) {
        if (res.rows.length > 0) {
          $scope.regTam3 = res.rows.length
          for (var i = 0; i < res.rows.length; i++) {
            $scope.data3.push(
              {
                idActivo: res.rows.item(i).idActivo,
                latitud: res.rows.item(i).latitud,
                longitud: res.rows.item(i).longitud,
                direccion: res.rows.item(i).direccion,
                barrio: res.rows.item(i).barrio,
                comuna: res.rows.item(i).comuna
              })
          }
        } else {
          console.log("No results found");
        }
        $ionicLoading.hide();
      }, function (err) {
        $ionicLoading.hide();
        console.error(err);
      });

      $scope.back = function () {
        $state.go("profileOperador", {}, {reload: true})
      }


    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });


    $scope.syncServer = function (type, activoID) {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        //console.log("SUBIR TIPO - " + type + " idActivo - " + activoID)
        var db = window.openDatabase('slap', '1.0', 'slapdb', 2000)

        $ionicLoading.show({
          template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
        });

        var user_login = JSON.parse(window.localStorage.getItem('usuario'))

        console.log("tipo . " + type)
        //REGISTRO PARA LUMINARIA Y REFLECTOR
        if (type === '1' || type === '3') {

          var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
          $scope.activo = {}
          var apoyoQ = "SELECT * FROM apoyo WHERE apoyo.activo_id = ? LIMIT 1"
          $scope.apoyo = {}
          var eiQ = "SELECT * FROM iluminacion WHERE iluminacion.apoyo_id = ?"
          $scope.ei = []
          var brzQ = "SELECT * FROM brazo WHERE brazo.ei_id = ? LIMIT 1"
          $scope.brz = {}
          var redQ = "SELECT * FROM red WHERE red.activo_id = ? LIMIT 1"
          $scope.red = {}


          var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
          var apId = 0
          console.log("Activo actual - " + activoID)
          $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
            if (res.rows.length > 0) {
              console.log("encuentra activo - ")
              $scope.activo = {
                longitud: res.rows.item(0).longitud,
                latitud: res.rows.item(0).latitud,
                direccion: res.rows.item(0).direccion,
                nomenclatura: res.rows.item(0).nomenclatura,
                barrio: res.rows.item(0).barrio,
                comuna: res.rows.item(0).comuna,
              }
              $cordovaSQLite.execute(db, apoyoQ, [activoID]).then(function (resAP) {
                if (resAP.rows.length > 0) {
                  apId = resAP.rows.item(0).id
                  console.log("encuentra apoyo - ")
                  $scope.apoyo = {
                    pertenece: resAP.rows.item(0).pertenece,
                    v_pertenece: resAP.rows.item(0).v_pertenece,
                    tipo: resAP.rows.item(0).tipo,
                    v_tipo: resAP.rows.item(0).v_tipo,
                    longitud: resAP.rows.item(0).longitud,
                    v_longitud: resAP.rows.item(0).v_longitud,
                    observacion: resAP.rows.item(0).observacion,
                    novedades: resAP.rows.item(0).novedades,
                  }

                  $cordovaSQLite.execute(db, eiQ, [resAP.rows.item(0).id]).then(function (resEi) {
                    if (resEi.rows.length > 0) {
                      var i = 0
                      for (i = 0; i < resEi.rows.length; i++) {
                        //console.log("CONTEO I: " + i + "Tipo - " + rows.item(i).eiTipo)

                        $scope.ei.push(
                          [{
                            tipo: resEi.rows.item(i).tipo,
                            tecnologia: resEi.rows.item(i).tecnologia,
                            v_tecnologia: resEi.rows.item(i).v_tecnologia,
                            nombre: resEi.rows.item(i).nombre,
                            potencia: resEi.rows.item(i).potencia,
                            v_potencia: resEi.rows.item(i).v_potencia,
                            foto: resEi.rows.item(i).foto,
                            observacion: resEi.rows.item(i).observacion,
                            novedades: resEi.rows.item(i).novedades,
                            serial: resEi.rows.item(i).serial
                          }, {
                            tipo: resEi.rows.item(i).brzTipo,
                            incorporado: resEi.rows.item(i).incorporado,
                            observaciones: resEi.rows.item(i).brzObs,
                            novedades: resEi.rows.item(i).brzNov
                          }])
                      }

                      $cordovaSQLite.execute(db, redQ, [activoID]).then(function (resRed) {
                        if (resRed.rows.length > 0) {
                          ("encuentra red - ")
                          $scope.red = {
                            tipo: resRed.rows.item(0).tipo,
                            v_tipo: resRed.rows.item(0).v_tipo,
                            distancia: resRed.rows.item(0).distancia,
                            v_distancia: resRed.rows.item(0).v_distancia,
                            aerea: resRed.rows.item(0).aerea,
                            observacion: resRed.rows.item(0).observacion,
                            novedades: resRed.rows.item(0).novedades
                          }

                          /*console.log("ACTIVO: " + JSON.stringify($scope.activo))
                           console.log("APOYO: " + JSON.stringify($scope.apoyo))
                           console.log("EI: " + $scope.ei)
                           console.log("RED: " + JSON.stringify($scope.red))*/

                          var user_login = JSON.parse(window.localStorage.getItem('usuario'))

                          var Digital = new Date()
                          var hours = Digital.getHours()
                          var minutes = Digital.getMinutes()
                          var seconds = Digital.getSeconds()

                          var hoy = new Date();
                          var dd = hoy.getDate();
                          var mm = hoy.getMonth() + 1;
                          var yyyy = hoy.getFullYear();

                          console.log('fecha_registro:' + yyyy + "-" + mm + "-" + dd + " --- " + 'hora:' + hours + ":" + minutes + " " + seconds)
                          $http({
                            method: 'POST',
                            url: CONFIG.APIURL + 'api/guardarLuminariaReflector',
                            data: {
                              activo: {
                                latitud: $scope.activo.latitud,
                                longitud: $scope.activo.longitud,
                                fecha_registro: yyyy + "-" + mm + "-" + dd,
                                hora: hours + ":" + minutes + ":" + seconds,
                                direccion: $scope.activo.direccion,
                                nomenclatura: $scope.activo.nomenclatura,
                                barrio: $scope.activo.barrio,
                                comuna: $scope.activo.comuna,
                              },
                              apoyo: $scope.apoyo,
                              EI: $scope.ei,
                              red: $scope.red,
                              token: user_login.token,
                              users_id: user_login.usuario.id
                            },
                          }).then(function successCallback(response) {
                            //console.log(JSON.stringify(response))
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                              title: '¡Felicidades!',
                              template: 'Se registrado el activo exitosamente..'
                            });
                            window.localStorage.removeItem('activo_actual')
                            window.localStorage.removeItem('reg_actual')
                            $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                            $cordovaSQLite.execute(db, "DELETE FROM apoyo WHERE apoyo.activo_id = ?", [activoID])
                            $cordovaSQLite.execute(db, "DELETE FROM brazo WHERE brazo.activo_id = ?", [activoID])
                            $cordovaSQLite.execute(db, "DELETE FROM iluminacion WHERE iluminacion.apoyo_id = ?", [apId])
                            $cordovaSQLite.execute(db, "DELETE FROM red WHERE red.activo_id = ?", [activoID])
                            $state.go("profileOperador", {}, {reload: true})
                          }, function errorCallback(response) {
                            //console.log(JSON.stringify(response))
                            $ionicPopup.alert({
                              title: '¡Ups, no se puede conectar con el servidor!',
                              template: 'No se ha detectado conexion con el servidor, los datos registrados se alamcenarán localmente.'
                            });
                            window.localStorage.removeItem('activo_actual')
                            window.localStorage.removeItem('reg_actual')
                            $state.go("profileOperador", {}, {reload: true})
                            $ionicLoading.hide();
                          });

                        }
                      })

                    }
                  })

                }
              })
            }
          })

          //FIN REGISTRO LUMINARIA Y REFLECTOR
        } else if (type === '2' || type === '4') {
          //REGISTRO FAROL Y TERREA
          //TODO: ENVIAR PETICION AL SERVIDOR
          var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
          $scope.activo = {}
          var eiQ = "SELECT * FROM iluminacion WHERE iluminacion.activo_id = ? LIMIT 1"
          $scope.ei = {}
          var redQ = "SELECT * FROM red WHERE red.activo_id = ? LIMIT 1"
          $scope.red = {}

          var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
          //console.log("Activo actual - " + activoID)
          $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
            if (res.rows.length > 0) {
              $scope.activo = {
                longitud: res.rows.item(0).longitud,
                latitud: res.rows.item(0).latitud,
                direccion: res.rows.item(0).direccion,
                nomenclatura: res.rows.item(0).nomenclatura,
                barrio: res.rows.item(0).barrio,
                comuna: res.rows.item(0).comuna,
              }
              $cordovaSQLite.execute(db, eiQ, [activoID]).then(function (resei) {
                if (resei.rows.length > 0) {
                  //c/onsole.log("encuentra eis - ")
                  $scope.ei = {
                    tipo: resei.rows.item(0).tipo,
                    tecnologia: resei.rows.item(0).tecnologia,
                    v_tecnologia: resei.rows.item(0).v_tecnologia,
                    nombre: resei.rows.item(0).nombre,
                    potencia: resei.rows.item(0).potencia,
                    v_potencia: resei.rows.item(0).v_potencia,
                    foto: resei.rows.item(0).foto,
                    observacion: resei.rows.item(0).observacion,
                    novedades: resei.rows.item(0).novedades,
                    serial: resei.rows.item(0).serial
                  }
                  //console.log("SELECTED -> " + resei.rows.item(0).tipo + "  ");


                  $cordovaSQLite.execute(db, redQ, [activoID]).then(function (resred) {
                    if (resred.rows.length > 0) {
                      $scope.red = {
                        tipo: resred.rows.item(0).tipo,
                        v_tipo: resred.rows.item(0).v_tipo,
                        distancia: resred.rows.item(0).distancia,
                        v_distancia: resred.rows.item(0).v_distancia,
                        aerea: resred.rows.item(0).aerea,
                        observacion: resred.rows.item(0).observacion,
                        novedades: resred.rows.item(0).novedades
                      }

                      //console.log("ACTIVO: " + JSON.stringify($scope.activo))
                      //console.log("EI: " + $scope.ei)
                      //console.log("RED: " + JSON.stringify($scope.red))
                      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
                      var ruta = ""
                      if ($scope.ei.tipo === 2) {
                        ruta = "guardarFarol"
                      } else if ($scope.ei.tipo === 4) {
                        ruta = "guardarTerreo"
                      }

                      var Digital = new Date()
                      var hours = Digital.getHours()
                      var minutes = Digital.getMinutes()
                      var seconds = Digital.getSeconds()

                      var hoy = new Date();
                      var dd = hoy.getDate();
                      var mm = hoy.getMonth() + 1;
                      var yyyy = hoy.getFullYear();

                      console.log('fecha_registro:' + yyyy + "-" + mm + "-" + dd + " --- " + 'hora:' + hours + ":" + minutes + " " + seconds)

                      $http({
                        method: 'POST',
                        url: CONFIG.APIURL + 'api/' + ruta,
                        data: {
                          activo: {
                            latitud: $scope.activo.latitud,
                            longitud: $scope.activo.longitud,
                            fecha_registro: yyyy + "-" + mm + "-" + dd,
                            hora: hours + ":" + minutes + ":" + seconds,
                            direccion: $scope.activo.direccion,
                            nomenclatura: $scope.activo.nomenclatura,
                            barrio: $scope.activo.barrio,
                            comuna: $scope.activo.comuna,
                          },
                          apoyo: $scope.apoyo,
                          EI: $scope.ei,
                          red: $scope.red,
                          brazo: $scope.brz,
                          token: user_login.token,
                          users_id: user_login.usuario.id
                        },
                      }).then(function successCallback(response) {
                        //console.log(JSON.stringify(response))
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                          title: '¡Felicidades!',
                          template: 'Se ha registrado el activo exitosamente.'
                        });
                        window.localStorage.removeItem('activo_actual')
                        window.localStorage.removeItem('reg_actual')
                        $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                        $cordovaSQLite.execute(db, "DELETE FROM iluminacion WHERE iluminacion.activo_id = ?", [activoID])
                        $cordovaSQLite.execute(db, "DELETE FROM red WHERE red.activo_id = ?", [activoID])
                        $state.go("profileOperador", {}, {reload: true})
                      }, function errorCallback(response) {
                        $ionicPopup.alert({
                          title: '¡Ups, parece que hay un error!',
                          template: 'Se ha persentado un inconveniente al conectar con el servidor, los datos quedaran almacenados en el dispositivo, por favor intentelo de nuevo mas tarde..'
                        });
                        $ionicLoading.hide();
                        window.localStorage.removeItem('activo_actual')
                        window.localStorage.removeItem('reg_actual')
                        $state.go("profileOperador", {}, {reload: true})
                      });
                    }
                  })
                }
              }), function (err) {
                console.error(err);
              }
            }
          }, function (err) {
            console.error(err);
          })
          //FIN REGISTRO FAROL Y TERREA
        } else if (type === '5') {
          //REGITRO TRAFO
          var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
          $scope.activo = {}
          var trafoQ = "SELECT * FROM trafo WHERE trafo.activo_id = ? LIMIT 1"
          $scope.trafo = {}
          var apoyoQ = "SELECT * FROM apoyo WHERE apoyo.trafo_id = ?"
          $scope.apoyo = []
          var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
          console.log("Activo actual - " + activoID)
          var user_login = JSON.parse(window.localStorage.getItem('usuario'))
          $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
            if (res.rows.length > 0) {
              console.log("activo")
              $cordovaSQLite.execute(db, trafoQ, [activoID]).then(function (restra) {
                if (restra.rows.length > 0) {
                  console.log("trafo")
                  $scope.trafo = {
                    potencia: restra.rows.item(0).potencia,
                    latitud: res.rows.item(0).latitud,
                    longitud: res.rows.item(0).longitud,
                    observacion: restra.rows.item(0).observacion,
                    serial: restra.rows.item(0).serial,
                    foto: restra.rows.item(0).foto,
                    trafo_validar: restra.rows.item(0).trafo_validar,
                    users_id: user_login.usuario.id,
                    direccion: res.rows.item(0).direccion,
                    nomenclatura: res.rows.item(0).nomenclatura,
                    barrio: res.rows.item(0).barrio,
                    comuna: res.rows.item(0).comuna,
                  }

                  $cordovaSQLite.execute(db, apoyoQ, [restra.rows.item(0).id]).then(function (resq) {
                    if (resq.rows.length > 0) {
                      console.log("apoyos")
                      for (var i = 0; i < resq.rows.length; i++) {
                        $scope.apoyo.push(
                          {
                            pertenece: resq.rows.item(0).pertenece,
                            v_pertenece: resq.rows.item(0).v_pertenece,
                            tipo: resq.rows.item(0).tipo,
                            v_tipo: resq.rows.item(0).v_tipo,
                            longitud: resq.rows.item(0).longitud,
                            v_longitud: resq.rows.item(0).v_longitud,
                            observacion: resq.rows.item(0).observacion,
                            novedades: resq.rows.item(0).novedades
                          })
                      }

                      $http({
                        method: 'POST',
                        url: CONFIG.APIURL + 'api/guardarTrafo',
                        data: {
                          trafo: $scope.trafo,
                          apoyo: $scope.apoyo,
                          token: user_login.token,
                          users_id: user_login.usuario.id
                        },
                      }).then(function successCallback(response) {
                        console.log(JSON.stringify(response))
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                          title: '¡Felicidades!',
                          template: 'Se registrado el activo exitosamente..'
                        });
                        window.localStorage.removeItem('activo_actual')
                        window.localStorage.removeItem('reg_actual')
                        $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                        $cordovaSQLite.execute(db, "DELETE FROM apoyo WHERE apoyo.trafo_id = ?", [restra.rows.item(0).id])
                        $cordovaSQLite.execute(db, "DELETE FROM trafo WHERE trafo.activo_id = ?", [activoID])
                        $state.go("profileOperador", {}, {reload: true})
                      }, function errorCallback(response) {
                        $ionicPopup.alert({
                          title: '¡Ups, parece que hay un error al conectar con el servidor!',
                          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
                        });
                        window.localStorage.removeItem('activo_actual')
                        window.localStorage.removeItem('reg_actual')
                        $ionicLoading.hide()
                        $state.go("profileOperador", {}, {reload: true})
                      });

                    }
                  }, function (err) {
                    console.error(err);
                  })
                }
              }, function (err) {
                console.error(err);
              })
            }
          }, function (err) {
            console.error(err);
          })

        }
        //FIN REGISTRO TRAFO
      } else {
        $ionicPopup.alert({
          title: '¡Parece que no tienes conexion!',
          template: 'Debes conectarte a internet para realizar la actualización de la información.'
        })
      }
    }
  })
//FIN CTRLS REGISTROS

