angular.module('starter.trafoControllers', [])

  .controller("trafoPotenciaCtrl", function ($scope, $state, $ionicHistory, $stateParams, $cordovaSQLite, $cordovaCamera, $ionicPopup) {
    console.log("tafo potencia")
    var loadData = function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({
        disableBack: true
      })
    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    $scope.data = {
      trafoFoto: "",
      trafoPotencia: "",
      trafoSerial: "",
      trafoValidar: false,
      trafoObservaciones: ""
    }
    $scope.pictureURl = ""

    /**
     * @function - abre la camara y captura una foto
     */
    $scope.tomarFoto = function () {
      var options = {
        destinationType: Camera.DestinationType.DATA_URL,
        encodingType: Camera.EncodingType.JPG,
        quality: 50,
        sourceType: 1
      }
      $cordovaCamera.getPicture(options)
        .then(function (data) {
          $scope.data.trafoFoto = data
          $scope.pictureURl = 'data:image/jpg;base64,' + data
        }, function (error) {
          console.log('camera error:' + angular.toJson(error))
        })
    }

    $scope.saveTrafo = function () {

      var valida = 0
      if ($scope.data.trafoValidar) {
        valida = 1
      }

      if ($scope.data.trafoFoto && $scope.data.trafoPotencia) {
        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea Continuar?',
          template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para continuar.'
        });
        confirmPopup.then(function (res) {
          var idTrafo = ''
          if (res) {

            var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
            var exQuery = "INSERT INTO trafo (potencia,trafo_validar,observacion,foto,serial,activo_id) VALUES (?,?,?,?,?,?)"
            $cordovaSQLite.execute(db, exQuery, [
              $scope.data.trafoPotencia,
              valida,
              $scope.data.trafoObservaciones,
              $scope.data.trafoFoto,
              $scope.data.trafoSerial,
              $stateParams.idActivo
            ])

            $cordovaSQLite.execute(db, "SELECT * FROM trafo WHERE trafo.activo_id = ? LIMIT 1", [$stateParams.idActivo]).then(function (res) {
              if (res.rows.length > 0) {

                $state.go("trafoApoyo",
                  {
                    'idTrafo': res.rows.item(0).id
                  })

              }
            }, function (err) {
              console.error(err);
            });

          } else {
            console.log('No desea continuar');
          }
        });
      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe llenar los campos para continuar.'
        })
      }
    }

  })

  .controller("trafoApoyoCtrl", function ($scope, $state, $stateParams, $ionicPopup, $ionicHistory) {
    console.log("tafo apoyo")
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    $ionicHistory.nextViewOptions({
      disableBack: true
    })

    $scope.dataApoyo = {
      quien: '',
      apValidaPertenece: false,
    }

    $scope.toTipoApoyo = function () {

      var valida = ""
      if ($scope.dataApoyo.apValidaPertenece) {
        valida = 1
      } else {
        valida = 0
      }

      if ($scope.dataApoyo.quien) {

        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea Continuar?',
          template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para continuar con el registro del Apoyo.'
        })
        confirmPopup.then(function (res) {
          if (res) {
            if ($scope.dataApoyo.quien === "Telecom") {
              $state.go("trafoApoyoLong",
                {
                  'idTrafo': $stateParams.idTrafo,
                  'apValidaPertenece': valida,
                  'apTipo': 'Concreto',
                  'apValidaTipo': 0,
                  'apPert': $scope.dataApoyo.quien,
                })
            } else {
              $state.go("trafoApoyoTipo",
                {
                  'idTrafo': $stateParams.idTrafo,
                  'apPert': $scope.dataApoyo.quien,
                  'apValidaPertenece': valida
                })
            }

          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe seleccionar a quien pertenece el apoyo para continuar'
        })
      }
    }
  })

  .controller("trafoApoyoTipoCtrl", function ($stateParams, $ionicPopup, $state, $scope, $ionicHistory) {
    //console.log("tafo tipo apoyo")
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    $scope.dataApoyo = {
      apPert: $stateParams.apPert,
      apValidaTipo: false
    }

    $scope.apoyo = {
      tipo: '',
    }

    $scope.toLong = function () {
      //alert("to long apoyo :/ " + JSON.stringify($scope.dataApoyo) + " -- -- " + JSON.stringify($scope.apoyo))
      var valid = ""
      if ($scope.dataApoyo.apValidaTipo) {
        valid = 1
      } else {
        valid = 0
      }

      if ($scope.apoyo.tipo) {

        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea Continuar?',
          template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para continuar con el registro del Apoyo.'
        })
        confirmPopup.then(function (res) {
          if (res) {
            $state.go("trafoApoyoLong",
              {
                'idTrafo': $stateParams.idTrafo,
                'apPert': $stateParams.apPert,
                'apValidaPertenece': $stateParams.apValidaPertenece,
                'apTipo': $scope.apoyo.tipo,
                'apValidaTipo': valid
              })
          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe seleccionar un tipo de Apoyo para continuar'
        })
      }
    }
  })

  .controller("trafoApoyoLongCtrl", function ($scope, $state, $stateParams, $ionicPopup, $ionicHistory) {
    //console.log("tafo long")

    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    $scope.dataApoyo = {
      quien: $stateParams.apPert,
      tipoApoyo: $stateParams.apTipo,
      apValidaLong: false
    }

    $scope.longApoyo = {
      longCens: '',
      exclusivo: '',
      fibra: '',
      telecom: ''
    }


    $scope.toBrazo = function () {

      var longitud = ''
      if ($scope.dataApoyo.quien === 'Cens') {
        longitud = $scope.longApoyo.longCens
      }
      if ($scope.dataApoyo.quien === 'Exclusivo A.P') {
        longitud = $scope.longApoyo.exclusivo
      }
      if ($scope.dataApoyo.quien === 'Fibra') {
        longitud = $scope.longApoyo.fibra
      }
      if ($scope.dataApoyo.quien === 'Telecom') {
        longitud = $scope.longApoyo.telecom
      }

      var valida = ''
      if ($scope.dataApoyo.apValidaLong) {
        valida = 1
      } else {
        valida = 0
      }

      var pert = 0
      if ($stateParams.apPert === "Cens") {
        pert = 1
      }
      if ($stateParams.apPert === "Telecom") {
        pert = 2
      }
      if ($stateParams.apPert === "Exclusivo A.P") {
        pert = 3
      }

      if (longitud) {

        var confirmPopup = $ionicPopup.confirm({
          title: '¿Desea Continuar?',
          template: 'Por favor, verifique que la información que envía esté correctamente diligenciada para continuar con el registro del Apoyo.'
        })
        confirmPopup.then(function (res) {
          if (res) {
            $state.go("trafoApoyoNovedades",
              {
                'idTrafo': $stateParams.idTrafo,
                'apPert': pert,
                'apValidaPertenece': $stateParams.apValidaPertenece,
                'apTipo': $stateParams.apTipo,
                'apValidaTipo': $stateParams.apValidaTipo,
                'apLong': longitud,
                'apValiLong': valida
              })
          } else {
            console.log('No desea continuar')
          }
        })

      } else {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe seleccionar una longitud del Apoyo para continuar'
        })
      }

    }

  })

  .controller("trafoApoyoNovedadesCtrl", function (userApi, $ionicHistory, $scope, $stateParams, $ionicPopup, $cordovaSQLite, $cordovaNetwork, $ionicLoading, CONFIG, $http, $state) {
    //console.log("tafo novedaes")

    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    $scope.data = {
      observaciones: ''
    }

    $scope.chk = {
      a: false,
      b: false,
      c: false,
      d: false,
      e: false,
      f: false,
    }

    var novedades = ""

    $scope.noAplica = function () {
      $scope.chk.a = false
      $scope.chk.b = false
      $scope.chk.c = false
      $scope.chk.d = false
      $scope.chk.e = false
      $scope.change()
    }


    $scope.change = function () {
      if ($scope.chk.a) {
        novedades += "Valla publicitaria,"
        $scope.chk.f = false
      }
      if ($scope.chk.b) {
        novedades += "Poste desplomado,"
        $scope.chk.f = false
      }
      if ($scope.chk.c) {
        novedades += "Poste quebrado,"
        $scope.chk.f = false
      }
      if ($scope.chk.d) {
        novedades += "Poste caído,"
        $scope.chk.f = false
      }
      if ($scope.chk.e) {
        novedades += "Poste corroído,"
        $scope.chk.f = false
      }
      if ($scope.chk.f) {
        novedades = "No aplica"
      }
    }

    var tipo = ''
    if ($stateParams.apTipo === "Concreto") {
      tipo = 1
    }
    if ($stateParams.apTipo === "Metalico") {
      tipo = 2
    }
    if ($stateParams.apTipo === "Fibra") {
      tipo = 3
    }
    if ($stateParams.apTipo === "Riel 8 Mts") {
      tipo = 4
    }
    if ($stateParams.apTipo === "Madera 8 Mts") {
      tipo = 5
    }

    $scope.finalizarApoyo = function () {

      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        userApi.isOpen().then(function (response) {
          if (response.data.status === "fail") {
            alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
            ionic.Platform.exitApp();
          }
        })
      }

      if (novedades) {

        var idApoyo = ''
        var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
        var exQuery = "INSERT INTO apoyo (pertenece, v_pertenece, tipo, v_tipo, longitud, v_longitud, observacion, novedades, activo_id, trafo_id) VALUES (?,?,?,?,?,?,?,?,?,?)"
        $cordovaSQLite.execute(db, exQuery, [
          $stateParams.apPert ,
          $stateParams.apValidaPertenece,
          tipo,
          $stateParams.apValidaTipo,
          $stateParams.apLong,
          $stateParams.apValiLong,
          $scope.data.observaciones,
          novedades,
          0,
          $stateParams.idTrafo
        ])

        var myPopup = $ionicPopup.show({
          title: '¿Desea Finalizar?',
          subTitle: 'Por favor verifique si desea finalizar el registro del trafo o registrar otro apoyo.',
          buttons: [
            {
              text: '<b>Otro Apoyo</b>',
              type: 'button-calm',
              onTap: function (e) {
                $state.go("trafoApoyo", {'idTrafo': $stateParams.idTrafo}, {reload: true})
              }
            },
            {
              text: '<b>Finalizar el Trafo</b>',
              type: 'button-positive',
              onTap: function (e) {

                var isOnline = $cordovaNetwork.isOnline()
                if (isOnline) {
                  //TODO: ENVIAR PETICION AL SERVIDOR
                  $ionicLoading.show({
                    template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
                  });
                  var activoQ = "SELECT * FROM activo WHERE activo.id = ? LIMIT 1"
                  $scope.activo = {}
                  var trafoQ = "SELECT * FROM trafo WHERE trafo.activo_id = ? LIMIT 1"
                  $scope.trafo = {}
                  var apoyoQ = "SELECT * FROM apoyo WHERE apoyo.trafo_id = ?"
                  $scope.apoyo = []
                  var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
                  var activoID = window.localStorage.getItem('activo_actual')
                  console.log("Activo actual - " + activoID)
                  var user_login = JSON.parse(window.localStorage.getItem('usuario'))
                  $cordovaSQLite.execute(db, activoQ, [activoID]).then(function (res) {
                    if (res.rows.length > 0) {
                      console.log("activo")
                      $cordovaSQLite.execute(db, trafoQ, [activoID]).then(function (restra) {
                        if (restra.rows.length > 0) {
                          console.log("trafo")
                          $scope.trafo = {
                            potencia: restra.rows.item(0).potencia,
                            latitud: res.rows.item(0).latitud,
                            longitud: res.rows.item(0).longitud,
                            observacion: restra.rows.item(0).observacion,
                            serial: restra.rows.item(0).serial,
                            foto: restra.rows.item(0).foto,
                            trafo_validar: restra.rows.item(0).trafo_validar,
                            users_id: user_login.usuario.id,
                            direccion: res.rows.item(0).direccion,
                            nomenclatura: res.rows.item(0).nomenclatura,
                            barrio: res.rows.item(0).barrio,
                          }

                          $cordovaSQLite.execute(db, apoyoQ, [restra.rows.item(0).id]).then(function (resq) {
                            if (resq.rows.length > 0) {
                              console.log("apoyos")
                              for (var i = 0; i < resq.rows.length; i++) {
                                $scope.apoyo.push(
                                  {
                                    pertenece: resq.rows.item(0).pertenece,
                                    v_pertenece: resq.rows.item(0).v_pertenece,
                                    tipo: resq.rows.item(0).tipo,
                                    v_tipo: resq.rows.item(0).v_tipo,
                                    longitud: resq.rows.item(0).longitud,
                                    v_longitud: resq.rows.item(0).v_longitud,
                                    observacion: resq.rows.item(0).observacion,
                                    novedades: resq.rows.item(0).novedades
                                  })
                              }

                              $http({
                                method: 'POST',
                                url: CONFIG.APIURL + 'api/guardarTrafo',
                                data: {
                                  trafo: $scope.trafo,
                                  apoyo: $scope.apoyo,
                                  token: user_login.token,
                                  users_id: user_login.usuario.id
                                },
                              }).then(function successCallback(response) {
                                console.log(JSON.stringify(response))
                                $ionicLoading.hide();
                                $ionicPopup.alert({
                                  title: '¡Felicidades!',
                                  template: 'Se registrado el activo exitosamente..'
                                });
                                window.localStorage.removeItem('activo_actual')
                                window.localStorage.removeItem('reg_actual')
                                $cordovaSQLite.execute(db, "DELETE FROM activo WHERE activo.id = ?", [activoID])
                                $cordovaSQLite.execute(db, "DELETE FROM apoyo WHERE apoyo.trafo_id = ?", [restra.rows.item(0).id])
                                $cordovaSQLite.execute(db, "DELETE FROM trafo WHERE trafo.activo_id = ?", [activoID])
                                $state.go("profileOperador", {}, {reload: true})
                              }, function errorCallback(response) {
                                $ionicPopup.alert({
                                  title: '¡Ups, parece que hay un error al conectar con el servidor!',
                                  template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
                                });
                                window.localStorage.removeItem('activo_actual')
                                window.localStorage.removeItem('reg_actual')
                                $ionicLoading.hide()
                                $state.go("profileOperador", {}, {reload: true})
                              });

                            }
                          }, function (err) {
                            console.error(err);
                          })
                        }
                      }, function (err) {
                        console.error(err);
                      })
                    }
                  }, function (err) {
                    console.error(err);
                  })

                } else {
                  $ionicPopup.alert({
                    title: '¡Ups, parece que no hay Conexion!',
                    template: 'No se ha detectado conexion a la red, los datos registrados se alamcenarán localmente.'
                  });
                  window.localStorage.removeItem('activo_actual')
                  window.localStorage.removeItem('reg_actual')
                  $state.go("profileOperador", {}, {reload: true})
                }

              }
            }
          ]
        });

      } else if ($stateParams.eiTipo === "reflector") {
        $ionicPopup.alert({
          title: '¡Ups hay un error!',
          template: 'Debe digitar los campos necesarios para finalizar y continuar'
        })
      }
    }
  })

