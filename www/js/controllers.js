angular.module('starter.controllers', [])

  .controller('LoginCtrl', function ($cordovaNetwork, $scope, $state, $ionicPopup, CONFIG, $ionicLoading, userApi, $cordovaDevice, $ionicHistory) {

    var loadData = function () {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    /**
     * @function - muestra el info de la aplicación
     */
    $scope.info = function () {
      var alertPopup = $ionicPopup.alert({
        title: 'Información de Aplicación!',
        template: 'Copyright © InseGroup - slap versión 1.9'
      });
    }

    //variables para los datos de inicio de sesión
    $scope.datLogin = {
      user: "",
      password: "",
      uuid: ""
    }

    document.addEventListener("deviceready", function () {
      $scope.datLogin.uuid = $cordovaDevice.getUUID();
      console.log($scope.datLogin.uuid)
    }, false);

    /**
     * @function - realiza el proceso de login y realiza petición al servidor
     */
    $scope.login = function () {
      //
      var isOnline = $cordovaNetwork.isOnline()
      if (isOnline) {
        if ($scope.datLogin.user === "" || $scope.datLogin.user === undefined) {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Debe ingresar el nombre de usuario.'
          });
          return
        }
        //
        if ($scope.datLogin.password === "" || $scope.datLogin.password === undefined) {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Debe ingresar la contraseña.'
          });
          return
        }

        //Petición al servidor para realizar el login
        $ionicLoading.show({
          template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
        });

        userApi.login('api/loginmovil', $scope.datLogin)
          .then(function (response) {

            console.log(JSON.stringify(response.data))
            window.localStorage.setItem('usuario', JSON.stringify(response.data))
            var user_login = JSON.parse(window.localStorage.getItem('usuario'))
            if (user_login.usuario.type === "Operador") {
              $state.go("profileOperador")
            } else if (user_login.usuario.type === "Supervisor") {
              $state.go("perfilSupervisor")
            }
            $ionicLoading.hide()

          }).catch(function (err) {
          console.log(JSON.stringify(err))
          $ionicLoading.hide()

          if (err.data === "invalid_user_or_password") {
            $ionicPopup.alert({
              title: '¡Ups, parece que hay un error!',
              template: 'El nombre de usuario o la contraseña no coinciden, por favor verifica e intentalo de nuevo.'

            });
          }

          if (err.data === "sistema_cerrado") {
            $ionicPopup.alert({
              title: '¡Ups, parece que hay un error!',
              template: 'El sistema se encuentra cerrado para realizar operaciones.'

            });
          }

          if (err.data === "error_tipo_usuario") {
            $ionicPopup.alert({
              title: '¡Ups, parece que hay un error!',
              template: 'No tiene permisos para ingresar a la aplicación.'

            });
          }

          if (err.data === "error_uuid") {
            $ionicPopup.alert({
              title: '¡Ups, parece que hay un error!',
              template: 'Ya se habia iniciado sesion en otro dispositivo, debe solicitar cambio de uuid al administrador para iniciar nuevamente.'

            });
          }
        });
      } else {
        $ionicPopup.alert({
          title: '¡Parece que no tiene conexión a internet!',
          template: 'Para realizar el inicio de sesión es necesario que el dispositivo cuente con acceso a la red. Por favor asegúrese de tener acceso a internet para iniciar sesión.'
        });
        ionic.Platform.exitApp();
      }
    }
  })

  .controller("InfoCtrl", function ($scope, $state, $cordovaSQLite, $ionicPopup) {
    $scope.data = {}

    $scope.toProfile = function () {
      $state.go("profileOperador")
    }

    $scope.logout = function () {

      var myPopup = $ionicPopup.show({

        template: '<input type="text" ng-model="data.wifi">',
        title: '¿Desea cerrar sesión?',
        subTitle: '¿Está segur@ que desea cerrar sesión? <br/><br/> Tenga en cuenta que al cerrar sesión se <b>BORRAN TODOS</b> los datos locales y no se podrán recuperar. <br/><br/> Si desea continuar por favor digite la palabra <b>logout</b>',
        scope: $scope,
        buttons: [
          {text: 'Cancelar'},
          {
            text: '<b>Cerrar Sesión</b>',
            type: 'button-positive',
            onTap: function (e) {
              if ($scope.data.wifi === "logout") {

                var db = window.openDatabase('slap', '1.0', 'slapdb', 2000)
                $cordovaSQLite.execute(db, "DELETE FROM 'activo'")
                $cordovaSQLite.execute(db, "DELETE FROM 'apoyo'")
                $cordovaSQLite.execute(db, "DELETE FROM 'brazo'")
                $cordovaSQLite.execute(db, "DELETE FROM 'iluminacion'")
                $cordovaSQLite.execute(db, "DELETE FROM 'red'")
                $cordovaSQLite.execute(db, "DELETE FROM 'trafo'")
                window.localStorage.removeItem('usuario')
                window.localStorage.removeItem('reg_actual')
                window.localStorage.removeItem('activo_actual')

                $ionicPopup.alert({
                  title: '¡Se ha cerrado sesión!',
                  template: 'Se ha cerrado la sesión actual, se han borrado los datos del usuario.'

                });

                $state.go("login", {}, {reload: true})
              } else {
                $ionicPopup.alert({
                  title: '¡Se ha cerrado sesión!',
                  template: 'Se ha cerrado la sesión actual, se han borrado los datos del usuario.'

                });
              }
            }
          }
        ]

      });

    }
  })

  .controller("InfoSupCtrl", function ($scope, $state, $cordovaSQLite, $ionicPopup) {
    $scope.data = {}

    $scope.toProfile = function () {
      $state.go("perfilSupervisor", {}, {reload: true})
    }

    $scope.logout = function () {

      var myPopup = $ionicPopup.show({

        template: '<input type="text" ng-model="data.wifi">',
        title: '¿Desea cerrar sesión?',
        subTitle: '¿Está segur@ que desea cerrar sesión? <br/><br/> Tenga en cuenta que al cerrar sesión se <b>BORRAN TODOS</b> los datos locales y no se podrán recuperar. <br/><br/> Si desea continuar por favor digite la palabra <b>logout</b>',
        scope: $scope,
        buttons: [
          {text: 'Cancelar'},
          {
            text: '<b>Cerrar Sesión</b>',
            type: 'button-positive',
            onTap: function (e) {
              if ($scope.data.wifi === "logout") {

                $ionicPopup.alert({
                  title: '¡Se ha cerrado sesión!',
                  template: 'Se ha cerrado la sesión actual, se han borrado los datos del usuario.'

                });

                $state.go("login", {}, {reload: true})
              } else {
                $ionicPopup.alert({
                  title: '¡Se ha cerrado sesión!',
                  template: 'Se ha cerrado la sesión actual, se han borrado los datos del usuario.'

                });
              }
            }
          }
        ]

      });

    }
  })

  .controller("ManualCtrl", function ($scope, $state, $stateParams, $ionicHistory) {
    $scope.$on('$stateChangeSuccess', function (event, toState) {
      $scope.imagen = $stateParams.num
    });

    $scope.back = function(){
      $ionicHistory.goBack();
    }
  })
