// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter',
  [
    'ionic',
    'starter.controllers',
    'starter.controllersOperador',
    'starter.EiController',
    'starter.apoyoController',
    'starter.redControllers',
    'starter.redFarolControllers',
    'starter.eiFT',
    'starter.trafoControllers',
    'starter.controllersSupervisor',
    'ngCordova',
    'starter.services'
  ])

  .constant('CONFIG', {
    APIURL: "http://inse.com.co/slap/public/",
    APIURL2: "http://inse.com.co/slap/public",
  })

  .run(function ($ionicPlatform, $cordovaSQLite) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

      var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
      $cordovaSQLite.execute(db, "CREATE TABLE activo (id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,latitud	TEXT,longitud	TEXT,users_id	INTEGER,sync_server	TEXT DEFAULT NO,finalizado	TEXT DEFAULT 'NO',direccion	TEXT,nomenclatura	TEXT,barrio	TEXT,comuna	TEXT)");
      $cordovaSQLite.execute(db, "CREATE TABLE apoyo(id	INTEGER PRIMARY KEY AUTOINCREMENT,pertenece	INTEGER,v_pertenece	INTEGER DEFAULT 0,tipo	INTEGER DEFAULT 0,v_tipo	INTEGER DEFAULT 0,longitud	TEXT,v_longitud	INTEGER DEFAULT 0,observacion	TEXT,novedades	TEXT,activo_id	INTEGER DEFAULT 0,trafo_id	INTEGER DEFAULT 0)");
      $cordovaSQLite.execute(db, "CREATE TABLE brazo(id	INTEGER PRIMARY KEY AUTOINCREMENT,tipo	INTEGER DEFAULT 0,v_tipo	INTEGER DEFAULT 0,incorporado	INTEGER DEFAULT 0,observacion	TEXT,novedades	TEXT,activo_id	INTEGER,ei_id	INTEGER)");
      $cordovaSQLite.execute(db, "CREATE TABLE iluminacion(id	INTEGER PRIMARY KEY AUTOINCREMENT,tipo	INTEGER DEFAULT 0,tecnologia	TEXT,v_tecnologia	INTEGER DEFAULT 0,nombre	TEXT,potencia	TEXT,v_potencia	INTEGER DEFAULT 0,foto	TEXT,observacion TEXT,novedades	TEXT,serial	TEXT,apoyo_id	INTEGER,activo_id	INTEGER DEFAULT 0)");
      $cordovaSQLite.execute(db, "CREATE TABLE red(id	INTEGER PRIMARY KEY AUTOINCREMENT,tipo	INTEGER DEFAULT 0,v_tipo	INTEGER DEFAULT 0,distancia	INTEGER DEFAULT 0,v_distancia	INTEGER DEFAULT 0,aerea	INTEGER DEFAULT 0,observacion	TEXT,novedades	TEXT,activo_id	INTEGER)");
      $cordovaSQLite.execute(db, "CREATE TABLE trafo (id	INTEGER PRIMARY KEY AUTOINCREMENT,potencia	TEXT,trafo_validar	INTEGER DEFAULT 0,observacion	TEXT,foto	TEXT,serial	TEXT,activo_id	INTEGER)");
    });

  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })

      .state("info", {
        url: '/info',
        templateUrl: 'templates/info.html',
        controller: 'InfoCtrl'
      })

      .state("infoSup", {
        url: '/infoSup',
        templateUrl: 'templates/infoSup.html',
        controller: 'InfoSupCtrl'
      })

      .state("manual", {
        url: '/manual/:num',
        templateUrl: 'templates/manual.html',
        controller: 'ManualCtrl'
      })

      //------------ RUTAS OPERADOR
      .state('profileOperador', {
        url: '/profileOperador',
        templateUrl: 'templates/operador/profile.html',
        controller: 'ProfileOperadorCtrl'
      })

      .state('newRegisterOperador', {
        url: '/newRegisterOperador',
        templateUrl: 'templates/operador/newRegisterOperador.html',
        controller: 'NewRegisterOperadorOperadorCtrl'
      })

      //--- RUTAS REGISTROS OP
      .state('registros', {
        url: '/registros',
        templateUrl: 'templates/operador/registros/registros.html',
        controller: 'registrosCtrl'
      })

      .state('estadisticas', {
        url: '/estadisticas',
        templateUrl: 'templates/operador/registros/estadisticas.html',
        controller: 'estadisticasCtrl'
      })

      .state('registrosLocales', {
        url: '/registrosLocales',
        templateUrl: 'templates/operador/registros/registrosLocales.html',
        controller: 'registrosLocalesCtrl'
      })

      //--- APOYO
      .state('registroApoyo_1', {
        url: '/registroApoyo_1/:idActivo/:eiTipo',
        templateUrl: 'templates/operador/apoyo/nuevoApoyo_1.html',
        controller: 'nuevoApoyo_1Ctrl'
      })

      //--- EI
      .state('eiTipoEI', {
        url: '/eiTipoEI/:idActivo/:idApoyo/:eiTipo',
        templateUrl: 'templates/operador/elementosIluminacion/eiTipoEI.html',
        controller: 'eiTipoEICtrl'
      })

      .state('eiFarolTerra', {
        url: '/eiTipoEI/:idActivo/:eiTipo',
        templateUrl: 'templates/operador/elementosIluminacion/ft/eiFT.html',
        controller: 'eiFTCtrl'
      })

      //--- Redes
      .state('redTipo', {
        url: '/redTipo/:idActivo',
        templateUrl: 'templates/operador/red/regisroRed.html',
        controller: 'regisroRedCtrl'
      })

      //-- REDES FAROL
      .state('redFarolTerra', {
        url: '/redFarolTerra/:idActivo',
        templateUrl: 'templates/operador/red/regisroRed.html',
        controller: 'tipoRedFarolCtrl'
      })

      //----RUTAS TRANSFORMADORES
      .state('trafoPotencia', {
        url: '/trafoPotencia/:idActivo',
        templateUrl: 'templates/operador/trafo/trafoPotencia.html',
        controller: 'trafoPotenciaCtrl'
      })

      .state('trafoApoyo', {
        url: '/trafoApoyo/:idTrafo',
        templateUrl: 'templates/operador/trafo/apoyo/nuevoApoyo_1.html',
        controller: 'trafoApoyoCtrl'
      })

      .state('trafoApoyoTipo', {
        url: '/trafoApoyoTipo/:idTrafo/:apPert/:apValidaPertenece',
        templateUrl: 'templates/operador/trafo/apoyo/tipoApoyo.html',
        controller: 'trafoApoyoTipoCtrl'
      })

      .state('trafoApoyoLong', {
        url: '/trafoApoyoLong/:idTrafo/:apPert/:apValidaPertenece/:apTipo/:apValidaTipo',
        templateUrl: 'templates/operador/trafo/apoyo/longitudApoyo.html',
        controller: 'trafoApoyoLongCtrl'
      })

      .state('trafoApoyoNovedades', {
        url: '/trafoApoyoNovedades/:idTrafo/:apPert/:apValidaPertenece/:apTipo/:apValidaTipo/:apLong/:apValiLong',
        templateUrl: 'templates/operador/trafo/apoyo/apNovedades.html',
        controller: 'trafoApoyoNovedadesCtrl'
      })


      //-------------- RUTAS SUPERVISOR
      .state('perfilSupervisor', {
        url: '/perfilSupervisor',
        templateUrl: 'templates/supervisor/profileSupervisor.html',
        controller: 'perfilSupervisorCtrl'
      })

      .state('regValidadcion', {
        url: '/regValidadcion',
        templateUrl: 'templates/supervisor/regValidadcion.html',
        controller: 'regValidadcionCtrl'
      })

      .state('validarEi', {
        url: '/validarEi/:data',
        templateUrl: 'templates/supervisor/validar.html',
        controller: 'validarEiCtrl'
      })

      .state('validarTrafo', {
        url: '/validarTrafo/:data',
        templateUrl: 'templates/supervisor/validarTrafo.html',
        controller: 'validarTrafoCtrl'
      })

      .state('operadores', {
        url: '/operadores',
        templateUrl: 'templates/supervisor/operadores.html',
        controller: 'operadoresController'
      })

    var user_login = JSON.parse(window.localStorage.getItem('usuario'))
    console.log(window.localStorage.getItem('usuario'))
    if (user_login) {
      if (user_login.usuario.type === "Operador") {
        $urlRouterProvider.otherwise('/profileOperador');
      } else if (user_login.usuario.type === "Supervisor") {
        $urlRouterProvider.otherwise('/perfilSupervisor');
      }
    } else {
      $urlRouterProvider.otherwise('/login');
    }


  })
