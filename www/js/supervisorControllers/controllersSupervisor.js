angular.module('starter.controllersSupervisor', [])

  .controller("perfilSupervisorCtrl", function ($scope, $state, $http, CONFIG, $ionicHistory, $ionicPopup, $ionicLoading) {
    var loadData1 = function () {
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()
      $ionicHistory.nextViewOptions({
        disableBack: true
      });

      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/isOpen',
        data: {
          token: user_login.token,
        },
      }).then(function successCallback(response) {

        console.log(JSON.stringify(response))
        if (response.status === "fail") {
          alert("El sistema esta cerrado para su uso, si desea más información puede contactar al administrador de la plataforma.")
          ionic.Platform.exitApp();
        }

      }, function errorCallback(response) {
        $ionicPopup.alert({
          title: '¡Ups, parece que hay un error al conectar con el servidor!',
          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
        });
      });

      $scope.imageProfile = CONFIG.APIURL2 + user_login.usuario.profile_image

      //$scope.updateInfo()

    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData1();
    });

    $scope.data = {
      meta_global: 0,
      realizado: 0,
      meta_hoy: 0,
      historico: 0,
    }
    $scope.ind = 0

    $scope.updateInfo = function () {

      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/indicadoresSupervisor',
        data: {
          'token': user_login.token,
          'users_id': user_login.usuario.id
        },
      }).then(function successCallback(response) {

        if (response.data.status === "ok") {
          $scope.ind = 1
          $scope.data.meta_global = response.data.msj.meta_global
          $scope.data.realizado = response.data.msj.realizado
          $scope.data.meta_hoy = response.data.msj.meta_hoy
          $scope.data.historico = response.data.msj.historico
        } else {

        }
        $ionicLoading.hide();
      }, function errorCallback(response) {
        $ionicPopup.alert({
          title: '¡Ups, parece que hay un error al conectar con el servidor!',
          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
        });
        $ionicLoading.hide();
      });
    }

    $scope.toValidacion = function () {
      $state.go("regValidadcion", {}, {reload: true})
    }

    $scope.toInfo = function () {
      $state.go("infoSup")
    }

    $scope.toOperadores = function () {
      $state.go("operadores")
    }

  })

  .controller("regValidadcionCtrl", function ($scope, $state, $ionicHistory, $http, $ionicLoading, CONFIG, $ionicPopup) {

    $scope.dataEI = {}
    $scope.validaEI = 0
    $scope.dataFT = {}
    $scope.validaFT = 0
    $scope.dataTrf = {}
    $scope.validaTrf = 0
    $scope.url = CONFIG.APIURL
    $scope.urlImageProfile = CONFIG.APIURL2

    var loadData = function () {
      $scope.dataEI = {}
      $scope.validaEI = 0
      $scope.dataFT = {}
      $scope.validaFT = 0
      $scope.dataTrf = {}
      $scope.validaTrf = 0
      $ionicHistory.clearCache()
      $ionicHistory.clearHistory()
      update()
    }

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      loadData();
    });

    var update = function () {
      $ionicLoading.show({
        template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
      });

      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/validacionesSupervisor',
        data: {
          'id': user_login.usuario.id,
        },
      }).then(function successCallback(response) {

        if (response.data.luminarias) {
          $scope.validaEI = 1
          $scope.dataEI = response.data.luminarias
          console.log(JSON.stringify(response.data.luminarias))
        }
        if (response.data.faroles) {
          $scope.validaFT = 1
          $scope.dataFT = response.data.faroles
        }
        //if (response.data.trafos) {
        //  $scope.dataTrf = response.data.trafos
        //  $scope.validaTrf = 1
        //}

        $ionicLoading.hide();

      }, function errorCallback(response) {
        $ionicPopup.alert({
          title: '¡Ups, parece que hay un error al conectar con el servidor!',
          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
        });
        $ionicLoading.hide();

      });

    }

    $scope.actData = function () {
      $scope.dataEI = {}
      $scope.validaEI = 0
      $scope.dataFT = {}
      $scope.validaFT = 0
      $scope.dataTrf = {}
      $scope.validaTrf = 0
      update()
    }

    $scope.back = function () {
      $state.go("perfilSupervisor", {}, {reload: true})
    }

    $scope.validar = function (item, tipo) {
      if (tipo === "ei") {
        $state.go('validarEi', {data: item}, {reload: true})
      }

      if (tipo === "trafo") {
        $state.go('validarTrafo', {data: item}, {reload: true})
      }
    }

  })

  .controller("validarEiCtrl", function ($scope, $state, $stateParams, CONFIG, $http, $ionicLoading, $ionicPopup) {
      var params = JSON.parse($stateParams.data)
      console.log("PARAMS - " + params.idActivo)

      $scope.url = CONFIG.APIURL
      $scope.urlImageProfile = CONFIG.APIURL2
      $scope.item = JSON.parse($stateParams.data)

      if (params.ilTipo === '1' || params.ilTipo === '3') {
        //Datos para enviar a editar el elemento de iluminacion reflector
        $scope.ei = {
          idActivo: params.idActivo,
          longitud: params.longitud,
          latitud: params.latitud,
          direccion: params.direccion,
          nomenclatura: params.nomenclatura,
          ilID: params.ilID,
          ilTipo: '',
          ilTecnologia: '',
          ilNombre: '',
          ilPotencia: '',
          ilObs: '',
          ilNov: '',
          ilSerial: '',
          idBrz: params.idBrz,
          brzTipo: '',
          brzIncorp: '',
          brzObs: '',
          brzNovedades: '',
          apId: params.apId,
          apPertenece: '',
          apTipo: '',
          apLongitud: '',
          apObs: '',
          apNovedades: '',
          redId: params.redId,
          redTipo: '',
          redDistancia: '',
          redArerea: '',
          redObs: '',
          redNovedades: '',
        }

        //Seleccion de la tecnologia
        $scope.tecnologia = {
          sodio: false,
          led: false,
          metal_halide: false,
          mercurio: false,
          luz_mixta: false
        }

        $scope.eiTecnologia = function () {
          if ($scope.tecnologia.sodio) {
            $scope.tecnologia = {
              led: false,
              metal_halide: false,
              mercurio: false,
              luz_mixta: false
            }
            $scope.ei.tecnologia = 'sodio'
            $scope.ft.tecnologia = 'sodio'
          }
          if ($scope.tecnologia.led) {
            $scope.tecnologia = {
              sodio: false,
              metal_halide: false,
              mercurio: false,
              luz_mixta: false
            }
            $scope.ei.tecnologia = 'led'
            $scope.ft.tecnologia = 'led'
          }
          if ($scope.tecnologia.metal_halide) {
            $scope.tecnologia = {
              sodio: false,
              led: false,
              mercurio: false,
              luz_mixta: false
            }
            $scope.ei.tecnologia = 'sodio'
          }
          if ($scope.tecnologia.mercurio) {
            $scope.tecnologia = {
              sodio: false,
              led: false,
              metal_halide: false,
              luz_mixta: false
            }
            $scope.ei.tecnologia = 'mercurio'
          }
          if ($scope.tecnologia.luz_mixta) {
            $scope.tecnologia = {
              sodio: false,
              led: false,
              metal_halide: false,
              mercurio: false
            }
            $scope.ei.tecnologia = 'luz mixta'
          }
        }

        $scope.luminaria = {
          potenciaSodioA: false,
          potenciaSodioB: false,
          potenciaSodioC: false,
          potenciaSodioD: false,
          potenciaLedA: false,
          potenciaLedB: false,
          potenciaLedC: false,
          potenciaLedD: false,
          potenciaLedA20: false,
          potenciaLedA30: false,
          potenciaLedA32: false,
          potenciaLedA35: false,
          potenciaLedA36: false,
          potenciaLedA40: false,
          potenciaLedA42: false,
          potenciaLedB45: false,
          potenciaLedB46: false,
          potenciaLedB47: false,
          potenciaLedB50: false,
          potenciaLedB55: false,
          potenciaLedB60: false,
          potenciaLedB65: false,
          potenciaLedC70: false,
          potenciaLedC77: false,
          potenciaLedC90: false,
          potenciaLedD100: false,
          potenciaLedD120: false,
          potenciaMH70: false,
          potenciaMH120: false,
          potenciaMH400: false,
          potenciaMer125: false,
          potenciaMer250: false,
          potenciaMer400: false,
          potenciaLmx: false,
        }

        $scope.eiPostsodio = function () {
          if ($scope.luminaria.potenciaSodioA) {
            $scope.luminaria.potenciaSodioB = false
            $scope.luminaria.potenciaSodioC = false
            $scope.luminaria.potenciaSodioD = false
            $scope.ei.ilNombre = 'A'
            $scope.ei.ilPotencia = '70 W'
          }
          if ($scope.luminaria.potenciaSodioB) {
            $scope.luminaria.potenciaSodioA = false
            $scope.luminaria.potenciaSodioC = false
            $scope.luminaria.potenciaSodioD = false
            $scope.ei.ilNombre = 'B'
            $scope.ei.ilPotencia = '150 W'
          }
          if ($scope.luminaria.potenciaSodioC) {
            $scope.luminaria.potenciaSodioA = false
            $scope.luminaria.potenciaSodioB = false
            $scope.luminaria.potenciaSodioD = false
            $scope.ei.ilNombre = 'C'
            $scope.ei.ilPotencia = '250 W'
          }
          if ($scope.luminaria.potenciaSodioD) {
            $scope.luminaria.potenciaSodioA = false
            $scope.luminaria.potenciaSodioB = false
            $scope.luminaria.potenciaSodioC = false
            $scope.ei.ilNombre = 'D'
            $scope.ei.ilPotencia = '400 W'
          }
        }

        $scope.eiPostLed = function () {
          if ($scope.luminaria.potenciaLedA) {
            $scope.luminaria.potenciaLedB = false
            $scope.luminaria.potenciaLedC = false
            $scope.luminaria.potenciaLedD = false
            $scope.ei.ilNombre = 'A'
          }
          if ($scope.luminaria.potenciaLedB) {
            $scope.luminaria.potenciaLedA = false
            $scope.luminaria.potenciaLedC = false
            $scope.luminaria.potenciaLedD = false
            $scope.ei.ilNombre = 'B'
          }
          if ($scope.luminaria.potenciaLedC) {
            $scope.luminaria.potenciaLedA = false
            $scope.luminaria.potenciaLedB = false
            $scope.luminaria.potenciaLedD = false
            $scope.ei.ilNombre = 'C'
          }
          if ($scope.luminaria.potenciaLedD) {
            $scope.luminaria.potenciaLedA = false
            $scope.luminaria.potenciaLedB = false
            $scope.luminaria.potenciaLedC = false
            $scope.ei.ilNombre = 'D'
          }
        }

        $scope.eiPotenciaLed = function () {
          if ($scope.luminaria.potenciaLedA20) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '20 W'
          }
          if ($scope.luminaria.potenciaLedA30) {
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '30 W'
          }
          if ($scope.luminaria.potenciaLedA32) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '32 W'
          }
          if ($scope.luminaria.potenciaLedA35) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '35 W'
          }
          if ($scope.luminaria.potenciaLedA36) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '36 W'
          }
          if ($scope.luminaria.potenciaLedA40) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '40 W'
          }
          if ($scope.luminaria.potenciaLedA42) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '42 W'
          }
          if ($scope.luminaria.potenciaLedB45) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '45 W'
          }
          if ($scope.luminaria.potenciaLedB46) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '46 W'
          }
          if ($scope.luminaria.potenciaLedB47) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '47 W'
          }
          if ($scope.luminaria.potenciaLedB50) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '50 W'
          }
          if ($scope.luminaria.potenciaLedB55) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '55 W'
          }
          if ($scope.luminaria.potenciaLedB60) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '60 W'
          }
          if ($scope.luminaria.potenciaLedB65) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '65 W'
          }
          if ($scope.luminaria.potenciaLedC70) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '70 W'
          }
          if ($scope.luminaria.potenciaLedC77) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '77 W'
          }
          if ($scope.luminaria.potenciaLedC90) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '90 W'
          }
          if ($scope.luminaria.potenciaLedD100) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD120 = false
            $scope.ei.ilPotencia = '100 W'
          }
          if ($scope.luminaria.potenciaLedD120) {
            $scope.luminaria.potenciaLedA30 = false
            $scope.luminaria.potenciaLedA20 = false
            $scope.luminaria.potenciaLedA32 = false
            $scope.luminaria.potenciaLedA35 = false
            $scope.luminaria.potenciaLedA36 = false
            $scope.luminaria.potenciaLedA40 = false
            $scope.luminaria.potenciaLedA42 = false
            $scope.luminaria.potenciaLedB45 = false
            $scope.luminaria.potenciaLedB46 = false
            $scope.luminaria.potenciaLedB47 = false
            $scope.luminaria.potenciaLedB50 = false
            $scope.luminaria.potenciaLedB55 = false
            $scope.luminaria.potenciaLedB60 = false
            $scope.luminaria.potenciaLedB65 = false
            $scope.luminaria.potenciaLedC70 = false
            $scope.luminaria.potenciaLedC77 = false
            $scope.luminaria.potenciaLedC90 = false
            $scope.luminaria.potenciaLedD100 = false
            $scope.ei.ilPotencia = '120 W'
          }
        }

        $scope.eiPotenciaMH = function () {
          if ($scope.luminaria.potenciaMH70) {
            $scope.luminaria.potenciaMH120 = false
            $scope.luminaria.potenciaMH400 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '70 W'
          }
          if ($scope.luminaria.potenciaMH120) {
            $scope.luminaria.potenciaMH70 = false
            $scope.luminaria.potenciaMH400 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '120 W'
          }
          if ($scope.luminaria.potenciaMH400) {
            $scope.luminaria.potenciaMH70 = false
            $scope.luminaria.potenciaMH120 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '400 W'
          }
        }

        $scope.eiPotenciaMer = function () {
          if ($scope.luminaria.potenciaMer125) {
            $scope.luminaria.potenciaMer250 = false
            $scope.luminaria.potenciaMer400 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '125 W'
          }
          if ($scope.luminaria.potenciaMer250) {
            $scope.luminaria.potenciaMer125 = false
            $scope.luminaria.potenciaMer400 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '250 W'
          }
          if ($scope.luminaria.potenciaMer400) {
            $scope.luminaria.potenciaMer125 = false
            $scope.luminaria.potenciaMer250 = false
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '400 W'
          }
        }

        $scope.eiPotenciaLMx = function () {
          if ($scope.luminaria.potenciaLmx) {
            $scope.ei.ilNombre = ''
            $scope.ei.ilPotencia = '160 W'
          }
        }

        $scope.brz = {
          tipo1: false,
          tipo2: false,
          tipoN: false,
          incorporado: false,
          noincorporado: false,
        }

        $scope.brzTipo = function () {
          if ($scope.brz.tipo1) {
            $scope.brz.tipo2 = false
            $scope.brz.tipoN = false
            $scope.ei.brzTipo = '1'
          }
          if ($scope.brz.tipo2) {
            $scope.brz.tipo1 = false
            $scope.brz.tipoN = false
            $scope.ei.brzTipo = '2'
          }
          if ($scope.brz.tipoN) {
            $scope.brz.tipo1 = false
            $scope.brz.tipo2 = false
            $scope.ei.brzTipo = '3'
          }
        }

        $scope.brzTipoInc = function () {
          if ($scope.brz.incorporado) {
            $scope.brz.noincorporado = false
            $scope.ei.brzIncorp = 1
          }
          if ($scope.brz.noincorporado) {
            $scope.brz.incorporado = false
            $scope.ei.brzIncorp = 0
          }
        }

        $scope.apPert = {
          cens: false,
          telecom: false,
          exclusivo: false,
        }

        $scope.apPertenece = function () {
          if ($scope.apPert.cens) {
            $scope.apPert.telecom = false
            $scope.apPert.exclusivo = false
            $scope.ei.apPertenece = 1
          }
          if ($scope.apPert.telecom) {
            $scope.apPert.cens = false
            $scope.apPert.exclusivo = false
            $scope.ei.apPertenece = 2
          }
          if ($scope.apPert.exclusivo) {
            $scope.apPert.cens = false
            $scope.apPert.telecom = false
            $scope.ei.apPertenece = 3
          }
        }

        $scope.apType = {
          concreto: false,
          metalico: false,
          fibra: false,
          riel: false,
          madera: false,
        }

        $scope.apTipo = function () {
          if ($scope.apType.concreto) {
            $scope.apType.metalico = false
            $scope.apType.fibra = false
            $scope.apType.riel = false
            $scope.apType.madera = false
            $scope.ei.apTipo = 1
          }
          if ($scope.apType.metalico) {
            $scope.apType.concreto = false
            $scope.apType.fibra = false
            $scope.apType.riel = false
            $scope.apType.madera = false
            $scope.ei.apTipo = 2
          }
          if ($scope.apType.fibra) {
            $scope.apType.concreto = false
            $scope.apType.metalico = false
            $scope.apType.riel = false
            $scope.apType.madera = false
            $scope.ei.apTipo = 3
          }
          if ($scope.apType.riel) {
            $scope.apType.concreto = false
            $scope.apType.metalico = false
            $scope.apType.fibra = false
            $scope.apType.madera = false
            $scope.ei.apTipo = 4
          }
          if ($scope.apType.madera) {
            $scope.apType.concreto = false
            $scope.apType.metalico = false
            $scope.apType.fibra = false
            $scope.apType.riel = false
            $scope.ei.apTipo = 5
          }
        }

        $scope.redComp = {
          cens: false,
          exclu: false,
        }

        $scope.redCompf = function () {
          if ($scope.redComp.cens) {
            $scope.redComp.exclu = false
            $scope.ei.redTipo = 1
          }
          if ($scope.redComp.exclu) {
            $scope.redComp.cens = false
            $scope.ei.redTipo = 2
          }
        }

        $scope.rfl = {
          sodio: false,
          led: false,
          metal_halide: false,
          mercurio: false,
        }

        $scope.rflTecnologia = function () {
          if ($scope.rfl.sodio) {
            $scope.rfl.led = false
            $scope.rfl.metal_halide = false
            $scope.rfl.mercurio = false
            $scope.ei.ilTecnologia = 'sodio'
          }
          if ($scope.rfl.led) {
            $scope.rfl.sodio = false
            $scope.rfl.metal_halide = false
            $scope.rfl.mercurio = false
            $scope.ei.ilTecnologia = 'led'
          }
          if ($scope.rfl.metal_halide) {
            $scope.rfl.sodio = false
            $scope.rfl.led = false
            $scope.rfl.mercurio = false
            $scope.ei.ilTecnologia = 'metal halide'
          }
          if ($scope.rfl.mercurio) {
            $scope.rfl.sodio = false
            $scope.rfl.led = false
            $scope.rfl.metal_halide = false
            $scope.ei.ilTecnologia = 'mercurio'
          }
        }


      }

      if (params.ilTipo === '2' || params.ilTipo === '4') {
        //Datos para enviar a editar el fatol terra
        $scope.ft = {
          idActivo: params.idActivo,
          longitud: params.longitud,
          latitud: params.latitud,
          direccion: params.direccion,
          nomenclatura: params.nomenclatura,
          //Luminaria
          ilID: params.ilID,
          ilTipo: '',
          ilTecnologia: '',
          ilNombre: '',
          ilPotencia: '',
          ilObs: '',
          ilNov: '',
          ilSerial: '',
          //Red
          redId: params.redId,
          redTipo: '',
          redDistancia: '',
          redArerea: '',
          redObs: '',
          redNovedades: '',
        }

        $scope.ftTec = {
          sodio: false,
          led: false,
        }

        $scope.ftTecnologia = function () {
          if ($scope.ftTec.sodio) {
            $scope.ftTec.led = false
            $scope.ft.ilTecnologia = 'sodio'
          }
          if ($scope.ftTec.led) {
            $scope.ftTec.sodio = false
            $scope.ft.ilTecnologia = 'led'
          }
        }

        $scope.ftPot = {
          potenciaSodio70: false,
          potenciaSodio150: false,
          potenciaSodio250: false,
          potenciaLed40: false,
          potenciaLed60: false,
        }

        $scope.ftPotencia = function () {
          if ($scope.ftPot.potenciaSodio70) {
            $scope.ftPot.potenciaSodio150 = false
            $scope.ftPot.potenciaSodio250 = false
            $scope.ftPot.potenciaLed40 = false
            $scope.ftPot.potenciaLed60 = false
            $scope.ft.ilNombre = ''
            $scope.ft.ilPotencia = '70 W'
          }
          if ($scope.ftPot.potenciaSodio150) {
            $scope.ftPot.potenciaSodio70 = false
            $scope.ftPot.potenciaSodio250 = false
            $scope.ftPot.potenciaLed40 = false
            $scope.ftPot.potenciaLed60 = false
            $scope.ft.ilNombre = ''
            $scope.ft.ilPotencia = '150 W'
          }
          if ($scope.ftPot.potenciaSodio250) {
            $scope.ftPot.potenciaSodio70 = false
            $scope.ftPot.potenciaSodio150 = false
            $scope.ftPot.potenciaLed40 = false
            $scope.ftPot.potenciaLed60 = false
            $scope.ft.ilNombre = ''
            $scope.ft.ilPotencia = '250 W'
          }
          if ($scope.ftPot.potenciaLed40) {
            $scope.ftPot.potenciaSodio70 = false
            $scope.ftPot.potenciaSodio150 = false
            $scope.ftPot.potenciaSodio250 = false
            $scope.ftPot.potenciaLed60 = false
            $scope.ft.ilNombre = ''
            $scope.ft.ilPotencia = '40 W'
          }
          if ($scope.ftPot.potenciaLed60) {
            $scope.ftPot.potenciaSodio70 = false
            $scope.ftPot.potenciaSodio150 = false
            $scope.ftPot.potenciaSodio250 = false
            $scope.ftPot.potenciaLed40 = false
            $scope.ft.ilNombre = ''
            $scope.ft.ilPotencia = '60 W'
          }
        }

        $scope.redComp = {
          cens: false,
          exclu: false,
        }

        $scope.redCompf = function () {
          if ($scope.redComp.cens) {
            $scope.redComp.exclu = false
            $scope.ft.redTipo = 1
          }
          if ($scope.redComp.exclu) {
            $scope.redComp.cens = false
            $scope.ft.redTipo = 2
          }
        }

      }

      $scope.back = function () {
        console.log(JSON.stringify($scope.ei))
        $state.go("regValidadcion", {}, {reload: true})
      }

      $scope.toSave = function (tipo) {
        var r = confirm("¿Seguro que desea validar la informacion diligenciada? \n\n Se marcara el elemento como validado.", "Confirmación");
        if (r == true) {
          if (tipo === '1' || tipo === '3') {
            $ionicLoading.show({
              template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
            });
            console.log("ILUMINACION - " + JSON.stringify($scope.ei))
            var user_login = JSON.parse(window.localStorage.getItem('usuario'))
            $http({
              method: 'POST',
              url: CONFIG.APIURL + 'api/saveValidacionSuperEI',
              data: {
                'id': user_login.usuario.id,
                'ei': $scope.ei,
                'tipo': tipo
              },
            }).then(function successCallback(response) {
              console.log("RESPUESTA - " + JSON.stringify(response))

              if (response.data.status === "ok") {
                alert('Se ha validado la información del elemento de iluminación.', "Información");
              }
              $ionicLoading.hide();

              $state.go("regValidadcion", {}, {reload: true})

            }, function errorCallback(response) {
              $ionicPopup.alert({
                title: '¡Ups, parece que hay un error al conectar con el servidor!',
                template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
              });
              $ionicLoading.hide();
            });

          }
          if (tipo === '2' || tipo === '4') {
            console.log("FAROL TERRA - " + JSON.stringify($scope.ft))
            $ionicLoading.show({
              template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
            });
            var user_login = JSON.parse(window.localStorage.getItem('usuario'))
            $http({
              method: 'POST',
              url: CONFIG.APIURL + 'api/saveValidacionSuperFT',
              data: {
                'id': user_login.usuario.id,
                'ft': $scope.ft,
                'tipo': tipo
              },
            }).then(function successCallback(response) {
              console.log(JSON.stringify(response))
              if (response.data.status === "ok") {
                alert('Se ha validado la información del elemento de iluminación.', "Información");
              }
              $ionicLoading.hide();

              $state.go("regValidadcion", {}, {reload: true})
            }, function errorCallback(response) {
              $ionicPopup.alert({
                title: '¡Ups, parece que hay un error al conectar con el servidor!',
                template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
              });
              $ionicLoading.hide();
            });

          }
        }
      }

    }
  )

  .controller("validarTrafoCtrl", function ($scope, $state, $stateParams, CONFIG, $http, $ionicLoading) {

    console.log("PARAMS TRF - " + JSON.stringify($stateParams))

    $scope.url = CONFIG.APIURL
    $scope.urlImageProfile = CONFIG.APIURL2
    $scope.item = JSON.parse($stateParams.data)
    var params = JSON.parse($stateParams.data)

    $scope.trf = {
      idTrafo: params.trafoID,
      longitud: params.longitud,
      latitud: params.latitud,
      direccion: params.direccion,
      nomenclatura: params.nomenclatura,
      trafoPotencia: '',
      apId: params.apId,
      apPertenece: '',
      apTipo: '',
      apLongitud: '',
    }

    $scope.toSave = function (tipo) {
      console.log("Trafo - " + JSON.stringify($scope.trf))
      $ionicLoading.show({
        template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
      });
      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/saveValidacionSuperTRF',
        data: {
          'id': user_login.usuario.id,
          'trf': $scope.trf,
          'tipo': tipo
        },
      }).then(function successCallback(response) {
        console.log(JSON.stringify(response))
        if (response.data.status === "ok") {
          alert('Se ha validado la información del elemento de iluminación.', "Información");
        }
        $ionicLoading.hide();
      }, function errorCallback(response) {
        $ionicPopup.alert({
          title: '¡Ups, parece que hay un error al conectar con el servidor!',
          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
        });
        $ionicLoading.hide();
      });
    }


    $scope.back = function () {
      $state.go("regValidadcion", {}, {reload: true})
    }
  })

  .controller('operadoresController', function ($scope, $http, CONFIG, $ionicLoading, $ionicPopup, $state) {

    $scope.$on('$stateChangeSuccess', function (event, toState) {
      $scope.cantOp = 0
      $scope.dataOp = {}
      $scope.url = CONFIG.APIURL
      $scope.urlImageProfile = CONFIG.APIURL2

      getOperadores();

    });

    var getOperadores = function () {

      $ionicLoading.show({
        template: '<i class="ion-load-a"></i> Se estan realizando el proceso de verificación, por favor espere.&nbsp; <br/>'
      });

      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/operadoresSupervisor',
        data: {
          'users_id': user_login.usuario.id,
        },
      }).then(function successCallback(response) {
        console.log(JSON.stringify(response.data.msj))
        if(response.data.msj.length > 0){
          $scope.dataOp = response.data.msj
          $scope.cantOp = 1
        }

        $ionicLoading.hide();

      }, function errorCallback(response) {
        $ionicPopup.alert({
          title: '¡Ups, parece que hay un error al conectar con el servidor!',
          template: 'Los datos quedarán almacenados en el dispositivo, por favor intente sincronizarlos mas tarde.'
        });
        $ionicLoading.hide();

      });

    }

    $scope.back = function () {
      $state.go("perfilSupervisor", {}, {reload: true})
    }

    $scope.refresh = function () {
      getOperadores()
    }

  })
