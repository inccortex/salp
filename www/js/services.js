angular.module('starter.services', [])

  .factory('userApi', ['$http', 'CONFIG', function ($http, CONFIG) {
    var service = {}
    /**
     * SERVICIO PARA LLAMAR AL LOGIN
     * @param url -> la ruta del login
     * @param username -> nombre de usuario digitado
     * @param password -> contraseña digitada
     */
    service.login = function (url, data) {
      return $http.post(CONFIG.APIURL + url, data)
    }

    service.isOpen = function () {
      var user_login = JSON.parse(window.localStorage.getItem('usuario'))
      return $http({
        method: 'POST',
        url: CONFIG.APIURL + 'api/isOpen',
        data: {
          token: user_login.token,
        },
      })
    }

    return service
  }])

  .factory('localBD', ['$cordovaSQLite', function ($cordovaSQLite) {
    var db = window.openDatabase('salp', '1.0', 'salpdb', 2000)
    var service = {}

    service.createActivo = function (activo) {
      var queryActivo = "INSERT INTO activo (longitud, latitud, users_id, direccion, nomenclatura, barrio, comuna) VALUES (?,?,?,?,?,?,?)"
      return $cordovaSQLite.execute(db, queryActivo,
        [
          activo.longitud,
          activo.latitud,
          activo.users_id,
          activo.direccion,
          activo.nomenclatura,
          activo.barrio,
          activo.comuna
        ])
    }


    service.createApoyo = function (apoyo) {
      var queeryApoyo = "INSERT INTO apoyo (pertenece, v_pertenece, tipo, v_tipo, longitud, v_longitud, observacion, novedades, activo_id, trafo_id) VALUES (?,?,?,?,?,?,?,?,?,?)"

      var vpert = 0
      if (apoyo.v_pertenece) {
        vpert = 1
      }

      var vtip = 0
      if (apoyo.v_tipo) {
        vtip = 1
      }

      var vlong = 0
      if (apoyo.v_longitud) {
        vlong = 1
      }

      return $cordovaSQLite.execute(db, queeryApoyo,
        [
          apoyo.pertenece,
          vpert,
          apoyo.tipo,
          vtip,
          apoyo.longitud,
          vlong,
          apoyo.observacion,
          apoyo.novedades,
          apoyo.activo_id,
          apoyo.trafo_id,
        ])
    }

    service.createEi = function (ei) {
      var queeryApoyo = "INSERT INTO iluminacion (tipo,tecnologia,v_tecnologia,nombre,potencia,v_potencia,foto,observacion,novedades,serial,apoyo_id,activo_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

      var vtec = 0
      if (ei.v_tecnologia) {
        vtec = 1
      }

      var vpot = 0
      if (ei.v_potencia) {
        vpot = 1
      }

      return $cordovaSQLite.execute(db, queeryApoyo,
        [
          ei.tipo,
          ei.tecnologia,
          vtec,
          ei.nombre,
          ei.potencia,
          vpot,
          ei.foto,
          ei.observacion,
          ei.novedades,
          ei.serial,
          ei.apoyo_id,
          ei.activo_id
        ])
    }

    service.createBrazo = function (brazo, eiID) {
      var queerybrz = "INSERT INTO brazo (tipo,v_tipo,incorporado,observacion,novedades,activo_id,ei_id) VALUES (?,?,?,?,?,?,?)"

      var vtip = 0
      if (brazo.v_tipo) {
        vtip = 1
      }

      return $cordovaSQLite.execute(db, queerybrz,
        [
          brazo.tipo,
          vtip,
          brazo.incorporado,
          brazo.observacion,
          brazo.novedades,
          brazo.activo_id,
          eiID,
        ])
    }

    service.createRed = function (red) {
      var queeryred = "INSERT INTO red (tipo,v_tipo,distancia,v_distancia,aerea,observacion,novedades,activo_id) VALUES (?,?,?,?,?,?,?,?)"

      var vtip = 0
      if (red.v_tipo) {
        vtip = 1
      }

      var vdist = 0
      if (red.v_distancia) {
        vdist = 1
      }

      return $cordovaSQLite.execute(db, queeryred,
        [
          red.tipo,
          vtip,
          red.distancia,
          vdist,
          red.aerea,
          red.observacion,
          red.novedades,
          red.activo_id
        ])
    }

    return service

  }])

